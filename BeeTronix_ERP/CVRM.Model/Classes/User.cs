﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CVRM.Model
{
    public class User
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public byte[] Image { get; set; }
        public int UserTypeID { get; set; }
        public virtual UserType UserType { get; set; }
        public ICollection<UserPermission> UserPermissions { get; set; }

        [NotMapped]
        public ImageSource ImageSource
        {
            get
            {
                return DBImage.ByteToImge(Image);
            }
        }
    }
}
