﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CVRM.Model
{
    public class CallPlace
    {
        public int ID { get; set; }
        public int CallID { get; set; }
        public virtual Call Call { get; set; }
        public int PlaceID { get; set; }
        public virtual Place Place { get; set; }

    }
}
