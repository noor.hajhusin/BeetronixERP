﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CVRM.Model
{
    [Table("CallPeople")]
    public class CallPerson
    {
        public int ID { get; set; }
        public string Details { get; set; }
        public int PersonID { get; set; }
        public virtual Person Person { get; set; }
        public int CallID { get; set; }
        public virtual Call Call { get; set; }
    }
}
