﻿using CVRM.Model.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CVRM.Model
{
    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public DateTime PublishDate { get; set; }
        public ProductStatus ProductStatus { get; set; }
        public int? ProductTypeID { get; set; }
        public virtual ProductType ProductType { get; set; }
        public virtual ICollection<CustomerProduct> CustomerProducts { get; set; }
        public virtual ICollection<EmployeeProduct> EmployeeProducts { get; set; }

        public int DepartmentID { get; set; }
        public virtual Department Department { get; set; }
    }
}
