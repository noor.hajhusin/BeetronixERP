﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.Model
{
    public class FileType
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string IconPath { get; set; }
        public string Extension { get; set; }
        public virtual ICollection<File> Files { get; set; }

    }
}
