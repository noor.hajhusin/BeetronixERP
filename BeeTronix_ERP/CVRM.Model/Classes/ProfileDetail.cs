﻿using System.Collections.Generic;

namespace CVRM.Model
{
    public class ProfileDetail
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int? ProfileDetailGroupID { get; set; }
        public ProfileDetailGroup ProfileDetailGroup { get; set; }
        public virtual ICollection<EmployeeProfileDetail> EmployeeProfileDetails { get; set; }

    }
    
}
