﻿using System.Collections.Generic;

namespace CVRM.Model
{
    public class EmployeeProfileDetail
    {
        public int ID { get; set; }
        public string Value { get; set; }
        public Datatype Datatype { get; set; }
        public int ProfileDetailID { get; set; }
        public ProfileDetail ProfileDetail { get; set; }
        public int EmployeeID { get; set; }
        public Employee Employee { get; set; }

    }
    
}
