﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.Model
{
    public class File
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public string Path { get; set; }
        public byte[] Data { get; set; }
        
        public int? FileTypeID { get; set; }
        public virtual FileType FileType { get; set; }
    }
}
