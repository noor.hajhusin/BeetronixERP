﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CVRM.Model
{
    public class CallEmployee
    {
        public int ID { get; set; }
        public string Details { get; set; }
        public int EmployeeID { get; set; }
        public virtual Employee Employee { get; set; }
        public int CallID { get; set; }
        public virtual Call Call { get; set; }
    }
}
