﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace CVRM.Model
{
    public class Call
    {
        public int ID { get; set; }
        public int FlowDirection { get; set; }
        public string Details { get; set; }
        public CallType CallType { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public TimeSpan Duration { get; set; }
        public bool IsClosed { get; set; }
        public int CustomerID { get; set; }
        public virtual Customer Customer { get; set; }
        public int? ReminderID { get; set; }
        [ForeignKey("ReminderID")]
        public virtual Call Reminder { get; set; }
        public virtual ICollection<CallPerson> CallPeople { get; set; }
        public virtual ICollection<CallEmployee> CallEmployees { get; set; }
        [InverseProperty("Reminder")]
        public virtual ICollection<Call> Reminders { get; set; }

        public virtual ICollection<CallPlace> CallPlaces { get; set; }

        [NotMapped]
        public string TagsDetails
        {
            get
            {
                string res = "";
                List<CallEmployee> ss = CallEmployees.ToList();
                if (ss != null)
                {
                    string tags = "Tagged Employees : ";
                    for (int i = 0; i < ss.Count; i++)
                    {
                        tags += ss[i].Employee.FullName + (i == ss.Count - 1 ? "" : " , ");
                    }
                    res += tags + "\n";
                }
                List<CallPerson> aa = CallPeople.ToList();
                if (aa != null)
                {
                    string tags = "Related People : ";
                    for (int i = 0; i < aa.Count; i++)
                    {
                        tags += aa[i].Person.FullName + (i == aa.Count - 1 ? "" : " , ");
                    }
                    res += tags;
                }
                return res;
            }
        }
        [NotMapped]
        public string Places
        {
            get
            {
                string res = "";
                List<CallPlace> ss = CallPlaces.ToList();
               // if (ss != null)
                {
                    for (int i = 0; i < ss.Count; i++)
                    {
                        res += "#"+ss[i].Place.Name + (i == ss.Count - 1 ? "" : "  ,  ");
                    }
                }
                return res;
            }
        }
    }
}
