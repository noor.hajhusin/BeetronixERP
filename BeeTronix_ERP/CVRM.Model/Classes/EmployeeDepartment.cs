﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.Model.Classes
{
    public class EmployeeDepartment

    {
        public int ID { get; set; }
        public virtual Employee  Employee { get; set; }
        public virtual Department Department { get; set; }
        public int EmployeeID { get; set; }
        public int DepartmentID { get; set; }

        public string Job { get; set; }
    }
}
