﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CVRM.Model
{
    public class CustomerActivity
    {
        public int ID { get; set; }
        public int ActivityID { get; set; }
        public virtual Activity Activity { get; set; }
        public int CustomerID { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
