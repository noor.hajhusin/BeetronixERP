﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CVRM.Model
{
    public class CustomerProduct
    {
        public int ID { get; set; }
        public string Details { get; set; }
        public int CustomerID { get; set; }
        public virtual Customer Customer { get; set; }
        public int ProductID { get; set; }
        public virtual Product Product { get; set; }
    }
}
