﻿using System.Collections.Generic;

namespace CVRM.Model
{
    public class PersonDetail
    {
        public int ID { get; set; }
        public string Value { get; set; }
        public string Details { get; set; }
        public int DetailID { get; set; }
        public virtual Detail Detail { get; set; }
        public int PersonID { get; set; }
        public virtual Person Person { get; set; }
    }
}
