﻿using System.Collections.Generic;

namespace CVRM.Model
{
    public class Detail
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<CustomerDetail> CustomerDetails { get; set; }
        public virtual ICollection<PersonDetail> PersonDetails { get; set; }

    }
    
}
