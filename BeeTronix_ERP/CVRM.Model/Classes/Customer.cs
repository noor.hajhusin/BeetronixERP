﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace CVRM.Model
{
    public class Customer
    {
        public int ID { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }
        public string Details { get; set; }
        public virtual ICollection<CustomerPerson> CustomerPeople { get; set; }
        public virtual ICollection<CustomerProduct> CustomerProducts { get; set; }
        public virtual ICollection<CustomerDetail> CustomerDetails { get; set; }
        public virtual ICollection<CustomerActivity> CustomerActivities { get; set; }

        [NotMapped]
        public string Activities
        {
            get
            {
                List<CustomerActivity> ss = CustomerActivities.ToList();
                string res = "";
                if (CustomerActivities == null) return "";
                for (int i=0;i< ss.Count;i++)
                {
                    res += ss[i].Activity.Name +(i==ss.Count-1?"":" , ");
                }
                return res;
            }
        }
        [NotMapped]
        public string ContactDetails
        {
            get
            {
                List<CustomerDetail> ss = CustomerDetails.ToList();
                string res = "";
                if (CustomerDetails == null) return "";
                for (int i = 0; i < ss.Count; i++)
                {
                    res += ss[i].Detail.Name + " : " + ss[i].Value + (i == ss.Count - 1 ? "" : "\n");
                }
                return res;
            }
        }
    }
}
