﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.Model.Classes
{
   public class EmployeeProduct
    {
        public int ID { get; set; }
        public virtual Product Product { get; set; }
        public virtual Employee Employee { get; set; }
        public int ProductID { get; set; }
        public int EmployeeID { get; set; }
        public virtual ContributeType ContributeType { get; set; }
        public int ContributeTypeID { get; set; }

    }
}
