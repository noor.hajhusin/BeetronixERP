﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.Model
{
    public class CustomerFile
    {
        public int ID { get; set; }
        public string Details { get; set; }
        public DateTime? Datetime { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? DeliverDate { get; set; }
        public bool IsFinally { get; set; }
        public int CustomerFileTypeID { get; set; }
        public virtual CustomerFileType CustomerFileType { get; set; }
        public int CustomerID { get; set; }
        public virtual Customer Customer { get; set; }
        public int? FileID { get; set; }
        public virtual File File { get; set; }
    }
}
