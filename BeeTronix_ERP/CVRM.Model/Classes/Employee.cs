﻿using CVRM.Model.Classes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace CVRM.Model
{
    public class Employee
    {
        public int ID { get; set; }
        public int BID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? DepartmentID { get; set; }
        public virtual Department Department { get; set; }
        public virtual ICollection<CallEmployee> CallEmployees { get; set; }
        public virtual ICollection<EmployeeProfileDetail> EmployeeProfileDetails { get; set; }
        public virtual ICollection<EmployeeProduct> EmployeeProducts { get; set; }

        public virtual ICollection<EmployeeDepartment> EmployeeDepartments { get; set; }

        [NotMapped]
        public string FullName { get { return $"{FirstName} {LastName}"; } }

    }
}
