﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CVRM.Model
{
    [Table("CustomerPeople")]
    public class CustomerPerson
    {
        public int ID { get; set; }
        public string Details { get; set; }
        public int CustomerID { get; set; }
        public virtual Customer Customer { get; set; }
        public int PersonID { get; set; }
        public virtual Person Person { get; set; }
        public int? JobID { get; set; }
        public virtual Job Job { get; set; }
    }
}
