﻿using System.Collections.Generic;

namespace CVRM.Model
{
    public class ProfileDetailGroup
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public virtual ICollection<ProfileDetail> ProfileDetails { get; set; }

    }
    
}
