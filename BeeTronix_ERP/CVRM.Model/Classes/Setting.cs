﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CVRM.Model
{
    [Table("Settings")]
    public class Setting
    {
        public int ID { get; set; }
        public string Key { get; set; }
        public string Details { get; set; }
        public object Value { get; set; }

    }
    
}
