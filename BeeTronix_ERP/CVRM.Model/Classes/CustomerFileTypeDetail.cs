﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.Model
{
    public class CustomerFileTypeDetail
    {
        public int ID { get; set; }
        public string Value { get; set; }
        public int CustomerFileTypeID { get; set; }
        public virtual CustomerFileType CustomerFileType { get; set; }
        public int FileTypeDetailID { get; set; }
        public virtual FileTypeDetail FileTypeDetail { get; set; }
    }
}
