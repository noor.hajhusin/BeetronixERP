﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace CVRM.Model
{
    [Table("People")]
    public class Person
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual ICollection<PersonDetail> PersonDetails { get; set; }
        public virtual ICollection<CustomerPerson> CustomerPeople { get; set; }
        public virtual ICollection<CallPerson> CallPeople { get; set; }

      

        [NotMapped]
        public string FullName { get { return $"{FirstName} {LastName}"; } }
        [NotMapped]
        public string ContactDetails
        {
            get
            {
                List<PersonDetail> ss = PersonDetails.ToList();
                string res = "";
                if (PersonDetails == null) return "";
                for (int i = 0; i < ss.Count; i++)
                {
                    res += ss[i].Detail.Name+" : "+ss[i].Value + (i == ss.Count - 1 ? "" : "  -  ");
                }
                return res;
            }
        }

    }
}
