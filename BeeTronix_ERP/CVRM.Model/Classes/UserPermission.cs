﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.Model
{
    public class UserPermission
    {
        public int ID { get; set; }
        public int Value { get; set; }
        public int UserID { get; set; }
        public virtual User User { get; set; }
        public int PermissionID { get; set; }
        public virtual Permission Permission { get; set; }
    }
}
