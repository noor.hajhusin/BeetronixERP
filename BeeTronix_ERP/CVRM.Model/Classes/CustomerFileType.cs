﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.Model
{
    public class CustomerFileType
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public Datatype Datatype { get; set; }
        public virtual ICollection<CustomerFile> CustomerFiles { get; set; }
        public virtual ICollection<CustomerFileTypeDetail> CustomerFileTypeDetails { get; set; }
    }
}
