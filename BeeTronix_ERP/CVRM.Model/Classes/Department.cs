﻿using CVRM.Model.Classes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace CVRM.Model
{
    public class Department
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
        public virtual ICollection<EmployeeDepartment> EmployeeDepartments { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        

    }
}
