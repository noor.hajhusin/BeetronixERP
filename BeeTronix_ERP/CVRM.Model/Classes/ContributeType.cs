﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.Model.Classes
{
   public class ContributeType

    {
        public int ID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<EmployeeProduct> EmployeeProducts { get; set; }
    }
}
