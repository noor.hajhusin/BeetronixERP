﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CVRM.Model
{
    public class Place
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public virtual ICollection<CallPlace> CallPlaces { get; set; }
    }
}
