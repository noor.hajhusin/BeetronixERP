﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CVRM.Model
{
    public class Job
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public virtual ICollection<CustomerPerson> CustomerPeople { get; set; }

       
    }
}
