﻿using System.Collections.Generic;

namespace CVRM.Model
{

    public class CustomerDetail
    {
        public int ID { get; set; }
        public string Value { get; set; }
        public string Details { get; set; }
        public int DetailID { get; set; }
        public virtual Detail Detail { get; set; }
        public int CustomerID { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
