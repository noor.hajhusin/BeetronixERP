﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.Model
{
    public static class Util
    {
        public static string ConnectionString { get { return getConnectionString(); } }

        private static string getConnectionString()
        {
            if (!System.IO.File.Exists(GetAppPath() + "\\cs.bee"))
                System.IO.File.WriteAllText(GetAppPath() + "\\cs.bee", "Data Source=.;Initial Catalog=CVRM;Integrated Security=True");
            return System.IO.File.ReadAllText(GetAppPath()+"\\cs.bee");
        }
        public static string GetAppPath()
        {
            return System.IO.Path.GetDirectoryName(System.Reflection.Assembly
                      .GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
        }
    }
}
