﻿using CVRM.Model.Classes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.Model
{
    public class CVRM_Context : DbContext
    {
        public CVRM_Context() : base(Util.ConnectionString)
        {
        }

        public virtual DbSet<Call> Calls { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerFile> CustomerFiles { get; set; }
        public virtual DbSet<File> Files { get; set; }
        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<PersonDetail> PersonDetails { get; set; }
        public virtual DbSet<Detail> Details { get; set; }
        public virtual DbSet<CustomerDetail> CustomerDetails { get; set; }
        public virtual DbSet<FileType> FileTypes { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Activity> Activities { get; set; }
        public virtual DbSet<CustomerActivity> CustomerActivities { get; set; }
        public virtual DbSet<CustomerProduct> CustomerProducts { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserType> UserTypes { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<UserPermission> UserPermission { get; set; }
        public virtual DbSet<CustomerFileType> CustomerFileTypes { get; set; }
        public virtual DbSet<CustomerFileTypeDetail> CustomerFileTypeDetails { get; set; }
        public virtual DbSet<FileTypeDetail> FileTypeDetails { get; set; }
        public virtual DbSet<CustomerPerson> CustomerPeople { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<CallPerson> CallPeople { get; set; }
        public virtual DbSet<CallEmployee> CallEmployees { get; set; }
        public virtual DbSet<ProductType> ProductTypes { get; set; }
        public virtual DbSet<Setting> Settings { get; set; }

        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Place> Places { get; set; }
        public virtual DbSet<CallPlace> CallPlaces { get; set; }


        public virtual DbSet<EmployeeProduct> EmployeeProducts { get; set; }
        public virtual DbSet<EmployeeDepartment> EmployeeDepartments { get; set; }
        public virtual DbSet<ContributeType> ContributeTypes { get; set; }
    }
}
