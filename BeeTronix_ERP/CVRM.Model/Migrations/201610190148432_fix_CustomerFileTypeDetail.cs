namespace CVRM.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fix_CustomerFileTypeDetail : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CustomerFileTypes", "FileTypeDetail_ID", "dbo.FileTypeDetails");
            DropIndex("dbo.CustomerFileTypes", new[] { "FileTypeDetail_ID" });
            AddColumn("dbo.Calls", "ReminderID", c => c.Int(nullable: true));
            CreateIndex("dbo.Calls", "ReminderID");
            AddForeignKey("dbo.Calls", "ReminderID", "dbo.Calls", "ID");
            DropColumn("dbo.CustomerFileTypes", "FileTypeDetail_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CustomerFileTypes", "FileTypeDetail_ID", c => c.Int());
            DropForeignKey("dbo.Calls", "ReminderID", "dbo.Calls");
            DropIndex("dbo.Calls", new[] { "ReminderID" });
            DropColumn("dbo.Calls", "ReminderID");
            CreateIndex("dbo.CustomerFileTypes", "FileTypeDetail_ID");
            AddForeignKey("dbo.CustomerFileTypes", "FileTypeDetail_ID", "dbo.FileTypeDetails", "ID");
        }
    }
}
