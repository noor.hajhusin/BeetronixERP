namespace CVRM.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class productType : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Products", "ProductTypeID", c => c.Int());
            CreateIndex("dbo.Products", "ProductTypeID");
            AddForeignKey("dbo.Products", "ProductTypeID", "dbo.ProductTypes", "ID");
            DropColumn("dbo.Products", "ProductType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "ProductType", c => c.Int());
            DropForeignKey("dbo.Products", "ProductTypeID", "dbo.ProductTypes");
            DropIndex("dbo.Products", new[] { "ProductTypeID" });
            DropColumn("dbo.Products", "ProductTypeID");
            DropTable("dbo.ProductTypes");
        }
    }
}
