namespace CVRM.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HrClasses : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Details = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EmployeeProfileDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Value = c.String(),
                        Datatype = c.Int(nullable: false),
                        ProfileDetailID = c.Int(nullable: false),
                        EmployeeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Employees", t => t.EmployeeID, cascadeDelete: true)
                .ForeignKey("dbo.ProfileDetails", t => t.ProfileDetailID, cascadeDelete: true)
                .Index(t => t.ProfileDetailID)
                .Index(t => t.EmployeeID);
            
            CreateTable(
                "dbo.ProfileDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ProfileDetailGroupID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ProfileDetailGroups", t => t.ProfileDetailGroupID)
                .Index(t => t.ProfileDetailGroupID);
            
            CreateTable(
                "dbo.ProfileDetailGroups",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Details = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Employees", "DepartmentID", c => c.Int());
            CreateIndex("dbo.Employees", "DepartmentID");
            AddForeignKey("dbo.Employees", "DepartmentID", "dbo.Departments", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProfileDetails", "ProfileDetailGroupID", "dbo.ProfileDetailGroups");
            DropForeignKey("dbo.EmployeeProfileDetails", "ProfileDetailID", "dbo.ProfileDetails");
            DropForeignKey("dbo.EmployeeProfileDetails", "EmployeeID", "dbo.Employees");
            DropForeignKey("dbo.Employees", "DepartmentID", "dbo.Departments");
            DropIndex("dbo.ProfileDetails", new[] { "ProfileDetailGroupID" });
            DropIndex("dbo.EmployeeProfileDetails", new[] { "EmployeeID" });
            DropIndex("dbo.EmployeeProfileDetails", new[] { "ProfileDetailID" });
            DropIndex("dbo.Employees", new[] { "DepartmentID" });
            DropColumn("dbo.Employees", "DepartmentID");
            DropTable("dbo.ProfileDetailGroups");
            DropTable("dbo.ProfileDetails");
            DropTable("dbo.EmployeeProfileDetails");
            DropTable("dbo.Departments");
        }
    }
}
