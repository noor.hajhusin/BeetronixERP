namespace CVRM.Model.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<CVRM.Model.CVRM_Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CVRM.Model.CVRM_Context context)
        {
            #region Jobs
            context.Jobs.AddOrUpdate(c => c.Name,
                    new Job { Name = "Manager" },
                    new Job { Name = "Manager Assistant" }
                    );
            #endregion
            #region FileTypes
            context.FileTypes.AddOrUpdate(c => c.Name,
                new FileType { Name = "PDF",Extension="PDF" },
                new FileType { Name = "Word", Extension = "doc , docx" },
                new FileType { Name = "Image", Extension = "jpg , png , jpeg" },
                new FileType { Name = "Excel", Extension = "xls , xlsx" }
                );
            context.SaveChanges();
            #endregion
            #region CustomerFileTypes
            context.CustomerFileTypes.AddOrUpdate(c => c.Name,
                new CustomerFileType { Name = "Invoice" },
                new CustomerFileType { Name = "Offer" },
                new CustomerFileType { Name = "Contract" }
                );
            context.SaveChanges();
            #endregion
            #region Details
            context.Details.AddOrUpdate(c => c.Name,
                new Detail { Name = "Mobile" },
                new Detail { Name = "Phone" },
                new Detail { Name = "Email" },
                new Detail { Name = "Facebook" },
                new Detail { Name = "Website" }
                );
            context.SaveChanges();
            #endregion
            #region activities
            context.Activities.AddOrUpdate(c => c.Name,
                new Activity { Name = "Institute" },
                new Activity { Name = "Charity assosiation" },
                new Activity { Name = "Governmental assosiation" },
                new Activity { Name = "Other" }
                );
            context.SaveChanges();
            #endregion
            #region UserTypes
            context.UserTypes.AddOrUpdate(
                p => p.Name,
                new UserType { Name = "Manager" },
                new UserType { Name = "Secretary" }
            );
            context.SaveChanges();
            #endregion
            #region Users
            context.Users.AddOrUpdate(
                    p => p.UserName,
                    new User
                    {
                        Name = "Manager",
                        UserName = "admin",
                        Password = "a",
                        UserTypeID = context.UserTypes.Where(c => c.Name == "Manager").FirstOrDefault().ID
                    }
                );
            context.SaveChanges();
            #endregion
            #region ProductTypes
            context.ProductTypes.AddOrUpdate(
                    p => p.Name,
                    new ProductType
                    {
                        Name = "Software"
                    },
                    new ProductType
                    {
                        Name = "Service"
                    }
                );
            context.SaveChanges();
            #endregion
            #region Settings
            context.Settings.AddOrUpdate(
                    p => p.Key,
                    new Setting { Key="CurrentUserID"},
                    new Setting { Key="KeepLoggedIn"}
                );
            context.SaveChanges();
            #endregion
        }
    }
}
