namespace CVRM.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductsManager : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmployeeDepartments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        EmployeeID = c.Int(nullable: false),
                        DepartmentID = c.Int(nullable: false),
                        Job = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Departments", t => t.DepartmentID, cascadeDelete: true)
                .ForeignKey("dbo.Employees", t => t.EmployeeID, cascadeDelete: true)
                .Index(t => t.EmployeeID)
                .Index(t => t.DepartmentID);
            
            CreateTable(
                "dbo.EmployeeProducts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductID = c.Int(nullable: false),
                        EmployeeID = c.Int(nullable: false),
                        ContributeTypeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ContributeTypes", t => t.ContributeTypeID, cascadeDelete: true)
                .ForeignKey("dbo.Employees", t => t.EmployeeID, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.ProductID)
                .Index(t => t.EmployeeID)
                .Index(t => t.ContributeTypeID);
            
            CreateTable(
                "dbo.ContributeTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Products", "DepartmentID", c => c.Int(nullable: false));
            CreateIndex("dbo.Products", "DepartmentID");
            AddForeignKey("dbo.Products", "DepartmentID", "dbo.Departments", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EmployeeProducts", "ProductID", "dbo.Products");
            DropForeignKey("dbo.EmployeeProducts", "EmployeeID", "dbo.Employees");
            DropForeignKey("dbo.EmployeeProducts", "ContributeTypeID", "dbo.ContributeTypes");
            DropForeignKey("dbo.Products", "DepartmentID", "dbo.Departments");
            DropForeignKey("dbo.EmployeeDepartments", "EmployeeID", "dbo.Employees");
            DropForeignKey("dbo.EmployeeDepartments", "DepartmentID", "dbo.Departments");
            DropIndex("dbo.EmployeeProducts", new[] { "ContributeTypeID" });
            DropIndex("dbo.EmployeeProducts", new[] { "EmployeeID" });
            DropIndex("dbo.EmployeeProducts", new[] { "ProductID" });
            DropIndex("dbo.Products", new[] { "DepartmentID" });
            DropIndex("dbo.EmployeeDepartments", new[] { "DepartmentID" });
            DropIndex("dbo.EmployeeDepartments", new[] { "EmployeeID" });
            DropColumn("dbo.Products", "DepartmentID");
            DropTable("dbo.ContributeTypes");
            DropTable("dbo.EmployeeProducts");
            DropTable("dbo.EmployeeDepartments");
        }
    }
}
