namespace CVRM.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "PublishDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Products", "ProductStatus", c => c.Int(nullable: false));
            AddColumn("dbo.ProductTypes", "Details", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductTypes", "Details");
            DropColumn("dbo.Products", "ProductStatus");
            DropColumn("dbo.Products", "PublishDate");
        }
    }
}
