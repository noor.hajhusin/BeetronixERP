namespace CVRM.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPlaceModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CallPlaces",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CallID = c.Int(nullable: false),
                        PlaceID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Calls", t => t.CallID, cascadeDelete: true)
                .ForeignKey("dbo.Places", t => t.PlaceID, cascadeDelete: true)
                .Index(t => t.CallID)
                .Index(t => t.PlaceID);
            
            CreateTable(
                "dbo.Places",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Details = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CallPlaces", "PlaceID", "dbo.Places");
            DropForeignKey("dbo.CallPlaces", "CallID", "dbo.Calls");
            DropIndex("dbo.CallPlaces", new[] { "PlaceID" });
            DropIndex("dbo.CallPlaces", new[] { "CallID" });
            DropTable("dbo.Places");
            DropTable("dbo.CallPlaces");
        }
    }
}
