namespace CVRM.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FileTypes", "Extension", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FileTypes", "Extension");
        }
    }
}
