namespace CVRM.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Activities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Details = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CustomerActivities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ActivityID = c.Int(nullable: false),
                        CustomerID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Activities", t => t.ActivityID, cascadeDelete: true)
                .ForeignKey("dbo.Customers", t => t.CustomerID, cascadeDelete: true)
                .Index(t => t.ActivityID)
                .Index(t => t.CustomerID);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        Details = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CustomerDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Value = c.String(),
                        Details = c.String(),
                        DetailID = c.Int(nullable: false),
                        CustomerID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Customers", t => t.CustomerID, cascadeDelete: true)
                .ForeignKey("dbo.Details", t => t.DetailID, cascadeDelete: true)
                .Index(t => t.DetailID)
                .Index(t => t.CustomerID);
            
            CreateTable(
                "dbo.Details",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PersonDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Value = c.String(),
                        Details = c.String(),
                        DetailID = c.Int(nullable: false),
                        PersonID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Details", t => t.DetailID, cascadeDelete: true)
                .ForeignKey("dbo.People", t => t.PersonID, cascadeDelete: true)
                .Index(t => t.DetailID)
                .Index(t => t.PersonID);
            
            CreateTable(
                "dbo.People",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CallPeople",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Details = c.String(),
                        PersonID = c.Int(nullable: false),
                        CallID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Calls", t => t.CallID, cascadeDelete: true)
                .ForeignKey("dbo.People", t => t.PersonID, cascadeDelete: true)
                .Index(t => t.PersonID)
                .Index(t => t.CallID);
            
            CreateTable(
                "dbo.Calls",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FlowDirection = c.Int(nullable: false),
                        Details = c.String(),
                        CallType = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Time = c.DateTime(nullable: false),
                        Duration = c.Time(nullable: false, precision: 7),
                        IsClosed = c.Boolean(nullable: false),
                        CustomerID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Customers", t => t.CustomerID, cascadeDelete: true)
                .Index(t => t.CustomerID);
            
            CreateTable(
                "dbo.CallEmployees",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Details = c.String(),
                        EmployeeID = c.Int(nullable: false),
                        CallID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Calls", t => t.CallID, cascadeDelete: true)
                .ForeignKey("dbo.Employees", t => t.EmployeeID, cascadeDelete: true)
                .Index(t => t.EmployeeID)
                .Index(t => t.CallID);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BID = c.Int(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CustomerPeople",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Details = c.String(),
                        CustomerID = c.Int(nullable: false),
                        PersonID = c.Int(nullable: false),
                        JobID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Customers", t => t.CustomerID, cascadeDelete: true)
                .ForeignKey("dbo.Jobs", t => t.JobID)
                .ForeignKey("dbo.People", t => t.PersonID, cascadeDelete: true)
                .Index(t => t.CustomerID)
                .Index(t => t.PersonID)
                .Index(t => t.JobID);
            
            CreateTable(
                "dbo.Jobs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Details = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CustomerProducts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Details = c.String(),
                        CustomerID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Customers", t => t.CustomerID, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.CustomerID)
                .Index(t => t.ProductID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Details = c.String(),
                        ProductType = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CustomerFiles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Details = c.String(),
                        Datetime = c.DateTime(),
                        CreationDate = c.DateTime(),
                        DeliverDate = c.DateTime(),
                        IsFinally = c.Boolean(nullable: false),
                        CustomerFileTypeID = c.Int(nullable: false),
                        CustomerID = c.Int(nullable: false),
                        FileID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Customers", t => t.CustomerID, cascadeDelete: true)
                .ForeignKey("dbo.CustomerFileTypes", t => t.CustomerFileTypeID, cascadeDelete: true)
                .ForeignKey("dbo.Files", t => t.FileID)
                .Index(t => t.CustomerFileTypeID)
                .Index(t => t.CustomerID)
                .Index(t => t.FileID);
            
            CreateTable(
                "dbo.CustomerFileTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Details = c.String(),
                        Datatype = c.Int(nullable: false),
                        FileTypeDetail_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FileTypeDetails", t => t.FileTypeDetail_ID)
                .Index(t => t.FileTypeDetail_ID);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Details = c.String(),
                        Path = c.String(),
                        Data = c.Binary(),
                        FileTypeID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.FileTypes", t => t.FileTypeID)
                .Index(t => t.FileTypeID);
            
            CreateTable(
                "dbo.FileTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Name = c.String(),
                        IconPath = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CustomerFileTypeDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Value = c.String(),
                        CustomerFileTypeID = c.Int(nullable: false),
                        FileTypeDetailID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CustomerFileTypes", t => t.CustomerFileTypeID, cascadeDelete: true)
                .ForeignKey("dbo.FileTypeDetails", t => t.FileTypeDetailID, cascadeDelete: true)
                .Index(t => t.CustomerFileTypeID)
                .Index(t => t.FileTypeDetailID);
            
            CreateTable(
                "dbo.FileTypeDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Details = c.String(),
                        Name = c.String(),
                        Datatype = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.UserPermissions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Value = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                        PermissionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Permissions", t => t.PermissionID, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.PermissionID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UserName = c.String(),
                        Password = c.String(),
                        Image = c.Binary(),
                        UserTypeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserTypes", t => t.UserTypeID, cascadeDelete: true)
                .Index(t => t.UserTypeID);
            
            CreateTable(
                "dbo.UserTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "UserTypeID", "dbo.UserTypes");
            DropForeignKey("dbo.UserPermissions", "UserID", "dbo.Users");
            DropForeignKey("dbo.UserPermissions", "PermissionID", "dbo.Permissions");
            DropForeignKey("dbo.CustomerFileTypeDetails", "FileTypeDetailID", "dbo.FileTypeDetails");
            DropForeignKey("dbo.CustomerFileTypes", "FileTypeDetail_ID", "dbo.FileTypeDetails");
            DropForeignKey("dbo.CustomerFileTypeDetails", "CustomerFileTypeID", "dbo.CustomerFileTypes");
            DropForeignKey("dbo.CustomerFiles", "FileID", "dbo.Files");
            DropForeignKey("dbo.Files", "FileTypeID", "dbo.FileTypes");
            DropForeignKey("dbo.CustomerFiles", "CustomerFileTypeID", "dbo.CustomerFileTypes");
            DropForeignKey("dbo.CustomerFiles", "CustomerID", "dbo.Customers");
            DropForeignKey("dbo.CustomerProducts", "ProductID", "dbo.Products");
            DropForeignKey("dbo.CustomerProducts", "CustomerID", "dbo.Customers");
            DropForeignKey("dbo.PersonDetails", "PersonID", "dbo.People");
            DropForeignKey("dbo.CustomerPeople", "PersonID", "dbo.People");
            DropForeignKey("dbo.CustomerPeople", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.CustomerPeople", "CustomerID", "dbo.Customers");
            DropForeignKey("dbo.CallPeople", "PersonID", "dbo.People");
            DropForeignKey("dbo.Calls", "CustomerID", "dbo.Customers");
            DropForeignKey("dbo.CallPeople", "CallID", "dbo.Calls");
            DropForeignKey("dbo.CallEmployees", "EmployeeID", "dbo.Employees");
            DropForeignKey("dbo.CallEmployees", "CallID", "dbo.Calls");
            DropForeignKey("dbo.PersonDetails", "DetailID", "dbo.Details");
            DropForeignKey("dbo.CustomerDetails", "DetailID", "dbo.Details");
            DropForeignKey("dbo.CustomerDetails", "CustomerID", "dbo.Customers");
            DropForeignKey("dbo.CustomerActivities", "CustomerID", "dbo.Customers");
            DropForeignKey("dbo.CustomerActivities", "ActivityID", "dbo.Activities");
            DropIndex("dbo.Users", new[] { "UserTypeID" });
            DropIndex("dbo.UserPermissions", new[] { "PermissionID" });
            DropIndex("dbo.UserPermissions", new[] { "UserID" });
            DropIndex("dbo.CustomerFileTypeDetails", new[] { "FileTypeDetailID" });
            DropIndex("dbo.CustomerFileTypeDetails", new[] { "CustomerFileTypeID" });
            DropIndex("dbo.Files", new[] { "FileTypeID" });
            DropIndex("dbo.CustomerFileTypes", new[] { "FileTypeDetail_ID" });
            DropIndex("dbo.CustomerFiles", new[] { "FileID" });
            DropIndex("dbo.CustomerFiles", new[] { "CustomerID" });
            DropIndex("dbo.CustomerFiles", new[] { "CustomerFileTypeID" });
            DropIndex("dbo.CustomerProducts", new[] { "ProductID" });
            DropIndex("dbo.CustomerProducts", new[] { "CustomerID" });
            DropIndex("dbo.CustomerPeople", new[] { "JobID" });
            DropIndex("dbo.CustomerPeople", new[] { "PersonID" });
            DropIndex("dbo.CustomerPeople", new[] { "CustomerID" });
            DropIndex("dbo.CallEmployees", new[] { "CallID" });
            DropIndex("dbo.CallEmployees", new[] { "EmployeeID" });
            DropIndex("dbo.Calls", new[] { "CustomerID" });
            DropIndex("dbo.CallPeople", new[] { "CallID" });
            DropIndex("dbo.CallPeople", new[] { "PersonID" });
            DropIndex("dbo.PersonDetails", new[] { "PersonID" });
            DropIndex("dbo.PersonDetails", new[] { "DetailID" });
            DropIndex("dbo.CustomerDetails", new[] { "CustomerID" });
            DropIndex("dbo.CustomerDetails", new[] { "DetailID" });
            DropIndex("dbo.CustomerActivities", new[] { "CustomerID" });
            DropIndex("dbo.CustomerActivities", new[] { "ActivityID" });
            DropTable("dbo.UserTypes");
            DropTable("dbo.Users");
            DropTable("dbo.UserPermissions");
            DropTable("dbo.Permissions");
            DropTable("dbo.FileTypeDetails");
            DropTable("dbo.CustomerFileTypeDetails");
            DropTable("dbo.FileTypes");
            DropTable("dbo.Files");
            DropTable("dbo.CustomerFileTypes");
            DropTable("dbo.CustomerFiles");
            DropTable("dbo.Products");
            DropTable("dbo.CustomerProducts");
            DropTable("dbo.Jobs");
            DropTable("dbo.CustomerPeople");
            DropTable("dbo.Employees");
            DropTable("dbo.CallEmployees");
            DropTable("dbo.Calls");
            DropTable("dbo.CallPeople");
            DropTable("dbo.People");
            DropTable("dbo.PersonDetails");
            DropTable("dbo.Details");
            DropTable("dbo.CustomerDetails");
            DropTable("dbo.Customers");
            DropTable("dbo.CustomerActivities");
            DropTable("dbo.Activities");
        }
    }
}
