﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.Model
{
    public enum ProductStatus { Available=0 , Suspended=1, UnderConstruction=2}
    public enum Datatype { Number=0, Text=1 , Date=2 , Time=3}

    public enum CallType { Incoming = 0, Outgoing = 1, Missed = 2, Meeting = 7, Support = 3, Visit = 4, Reminder = 6, Social=8 }
}
