﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace ProductsManager.VMs.Product
{
    class ProductVM : BaseVM
    {
        #region cotr 
        public ProductVM()
        {

        }
        #endregion
        #region properties

        public List<CVRM.Model.Product> FilteredProducts
        {
            get
            {
                if (Searching) return Products.Where(c => c.Name.Contains(SearchText)).ToList();
                else { return Products.ToList(); }
            }
        }

        public ObservableCollection<CVRM.Model.Product> products;
        public ObservableCollection<CVRM.Model.Product> Products
        {
            get { return products; }
            set
            {
                products = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredProducts");
            }
        }
        private object selectedProduct;

        public object SelectedProduct
        {
            get { return selectedProduct; }
            set
            {
                selectedProduct = value;
                OnPropertyChanged("SelectedProduct");
            }
        }

        private bool searching;
        public bool Searching
        {
            get { return searching; }
            set
            {
                searching = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredProducts");
            }
        }
        private string searchText;

        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
                OnPropertyChanged();
            }
        }
        #endregion


        #region Commands
        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>
            {
                //0-addNewProduct
                new CommandVM(new RelayCommand (param=>addNewProduct())),
                //1-update
                new CommandVM(new RelayCommand (param=>update())),
                //2-deleteSelected
                new CommandVM(new RelayCommand (param=>deleteSelected())),
                //3-search
                new CommandVM(new RelayCommand (param=>search())),
                //4-cancelCearch
                new CommandVM(new RelayCommand (param=>cancelSearch())),
           
            };
        }

        #endregion
        #region data
        public override void getData()
        {
            Products = new ObservableCollection<CVRM.Model.Product>(Context.Products.Include(c => c.ProductType).AsEnumerable());
            OnPropertyChanged("FilteredProducts");
        }

        public override void removeData()
        {
            Context = null;
        }
        #endregion

        #region Methods 
        private void search()
        {
            if (SearchText != null && SearchText.Length > 0) Searching = true;
        }
        void addNewProduct()
        {
          
        }
        private void cancelSearch()
        {
            SearchText = "";
            Searching = false;
        }
        void update()
        {
           
        }
        void deleteSelected()
        {
            if (SelectedProduct == null) return;

        }
        #endregion
    }
}
