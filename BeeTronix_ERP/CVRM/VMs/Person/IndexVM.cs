﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CVRM.VMs.Person
{
    public class IndexVM : BaseVM
    {
        #region ctor

        public IndexVM()
        {

        }
        #endregion

        #region Properties

        public ObservableCollection<CVRM.Model.Person> FilteredPeople
        {
            get
            {
                if (Searching) return new ObservableCollection<Model.Person>();
                else { return People;}
            }
        }
        public ObservableCollection<CVRM.Model.Person> people;
        public ObservableCollection<CVRM.Model.Person> People
        {
           get
            {
                return People;
            }
            set
            {
                people = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredPeople");
            }
        }
        private object selectedPerson;

        public object SelectedPerson
        {
            get { return selectedPerson; }
            set
            {
                selectedPerson = value;
                OnPropertyChanged();
            }
        }

        private bool searching;

        public bool Searching
        {
            get { return searching; }
            set
            {
                searching = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredPeople");
            }
        }
        private string searchText;

        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
                OnPropertyChanged();
            }
        }


        #endregion

        #region commands

        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>
            {
                //0-addNewPerson
                new CommandVM(new RelayCommand (param=>addNewPerson())),
                //1-update
                new CommandVM(new RelayCommand (param=>update())),
                //2-deleteSelected
                new CommandVM(new RelayCommand (param=>deleteSelected())),
                //3-search
                new CommandVM(new RelayCommand (param=>search())),
                //4-cancelSearch
                new CommandVM(new RelayCommand (param=>cancelSearch())),
            };
        }

        #endregion

        #region IData interface
        public override void getData()
        {
           
            People = new ObservableCollection<Model.Person>(Context.People.AsEnumerable());
            OnPropertyChanged("FilteredPeople");
        }

        public override void removeData()
        {
            Context = null;
        }
        #endregion

        #region Methods
        void addNewPerson()
        {
            ViewUC(() => new Views.Person.AddEdit()
            {
                ID = $"Person0",
                Title = $"New Person"
            });
        }

        private void search()
        {
           if(SearchText.Length>0) Searching = true;
        }

        private void cancelSearch()
        {
            SearchText = "";
            Searching = false;
        }
        void update()
        {
            if (SelectedPerson == null) return;
            ViewUC(() => new Views.Person.AddEdit(SelectedPerson as Model.Person)
            {
                ID = $"Person{((Model.Person)selectedPerson).ID}",
                Title = $"Person {((Model.Person)selectedPerson).FirstName}"
            });
        }
        void deleteSelected()
        {
            if (SelectedPerson == null) return;

        }
        #endregion
    }
}
