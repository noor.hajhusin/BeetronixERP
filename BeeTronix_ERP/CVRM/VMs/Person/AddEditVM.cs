﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.VMs.Person
{
    public class AddEditVM : BaseVM
    {
        #region cotr
        public AddEditVM()
        {
            Person = new Model.Person() {};
        }
        public AddEditVM(Model.Person Person)
        {
            this.Person = Context.People.Find(Person.ID);
        }
        #endregion

        #region Properties
        private Model.Person person;
        public Model.Person Person
        {
            private set
            {
                person = value;
                OnPropertyChanged("Person");
            }
            get
            {
                return person;
            }
        }


        private Model.Job job;
        public Model.Job Job
        {
            private set
            {
                job = value;
                OnPropertyChanged("Job");
            }
            get
            {
                return job;
            }
        }

        public ObservableCollection<Model.Job> Jobs { get; set; }

        #endregion

        #region Commands
        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>
            {
                //0-save&Close
                new CommandVM(new RelayCommand (param=>saveClose())),
                //1-cancel
                new CommandVM(new RelayCommand (param=>cancel())),


            };
        }
        #endregion

        #region Methods

        async void saveClose()
        {
            LoadingMessage = "Saving ...";
            startLoading();
            await Task.Run(() =>
            {
                if (Person.ID > 0) modifiyEntity(Person);
                else addEntity(Person);

            });
            stopLoading();
            closeTab(true);
        }

        void cancel()
        {
            closeTab(false);
        }
        #endregion

        public override void getData()
        {
            Jobs = new ObservableCollection<Model.Job>(Context.Jobs.AsEnumerable());
        }

        public override void removeData()
        {
   
        }
    }
}
