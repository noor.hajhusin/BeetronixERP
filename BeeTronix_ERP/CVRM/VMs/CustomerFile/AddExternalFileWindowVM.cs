﻿using CVRM.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.VMs.CustomerFile
{
    public class AddExternalFileWindowVM : BaseVM<AddExternalFileWindowVM>
    {
        #region Properties
        private Model.CustomerFile customerFile;

        public Model.CustomerFile CustomerFileObject
        {
            get { return customerFile; }
            set { customerFile = value; }
        }

        public ObservableCollection<Model.Customer> customers;
        public ObservableCollection<Model.Customer> Customers
        {
            get { return customers; }
            set
            {
                customers = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<CustomerFileType> CustomerFileTypes
        {
            get;set;
        }
        private ObservableCollection<CVRM.Model.FileType> fileTypes;

        public ObservableCollection<CVRM.Model.FileType> FileTypes
        {
            get { return fileTypes; }
            set { fileTypes = value; OnPropertyChanged(); }
        }
        private object selectedCustomer;

        public object SelectedCustomer
        {
            get { return selectedCustomer; }
            set
            {
                selectedCustomer = value;
                OnPropertyChanged();
            }
        }
        private bool saveFileData;

        public bool SaveFileData
        {
            get { return saveFileData; }
            set
            {
                saveFileData = value;
                OnPropertyChanged();
            }
        }

        string fileName;

        #endregion
        #region Ctors
        public AddExternalFileWindowVM(string fileName)
        {
            this.fileName = fileName;
        }
        #endregion
        #region IData
        public override void getData()
        {
            Customers = new ObservableCollection<Model.Customer>(Context.Customers.AsEnumerable());
            CustomerFileTypes = new ObservableCollection<CustomerFileType>(Context.CustomerFileTypes.AsEnumerable());
            FileTypes = new ObservableCollection<FileType>(Context.FileTypes.AsEnumerable());
            CustomerFileObject = new Model.CustomerFile {Datetime=DateTime.Now };
            CustomerFileObject.File = new Model.File { Name = System.IO.Path.GetFileNameWithoutExtension(fileName), Path = fileName };
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            CustomerFileObject.File.Data = new byte[fs.Length];
            fs.Read(CustomerFileObject.File.Data, 0, System.Convert.ToInt32(fs.Length));
            fs.Close();
            string ext = System.IO.Path.GetExtension(fileName).Remove(0, 1);
            FileType ft = FileTypes.Where(c => c.Extension.ToUpper().Contains(ext.ToUpper())).FirstOrDefault();
            CustomerFileObject.File.FileType = ft == null ? CustomerFileObject.File.FileType : ft;
            SaveFileData = true;
            OnPropertyChanged("SaveFileData");
            OnPropertyChanged("CustomerFileObject");
            OnPropertyChanged("CustomerFileTypes");

        }

        public override void removeData()
        {

        }
        #endregion
        #region Commands
        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>
            {
                new CommandVM(new RelayCommand(param=>save())),
                new CommandVM(new RelayCommand(param=>cancel())),
            };
        }

        private void save()
        {
            try
            {
                if (CustomerFileObject.Customer == null) return;
                if (SaveFileData)
                    if (CustomerFileObject.File.Data == null) CustomerFileObject.File.Data = new byte[] { };
                addEntity(CustomerFileObject);
                App.Current.Shutdown();
            }
            catch (Exception ee)
            {
                System.Windows.MessageBox.Show(ee.Message);
                if(ee.InnerException.InnerException!=null)
                System.Windows.MessageBox.Show(ee.InnerException.InnerException.Message);
            }
        }

        private void cancel()
        {
            App.Current.Shutdown();
        }
        #endregion
    }
}
