﻿using System;
using System.Collections.Generic;
using System.Linq;
using CVRM.Model;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using MahApps.Metro.Controls;
using System.IO;
using System.Diagnostics;
using CVRM.VMs.GUITools.CustomControls;
using CVRM.VMs.DialogTools;
using System.Collections.Specialized;

namespace CVRM.VMs
{
    public enum Language { Arabic, English }
    public interface IData
    {
        /// <summary>
        /// This method is being Called when the VM load Data from Database
        /// </summary>
        void getDarta();
    }

    /// <summary>
    /// Used as the Base class for all VM classes in the Project 
    /// represent the layer that interact with the View
    /// </summary>
    public abstract class BaseVM : DependencyObject, INotifyPropertyChanged, IData
    {
        #region Fields
        #region Constants
        const string GUI_Namespace = "CVRM.GUIs.";
        const string GUI_Assembly = "CVRM";
        #endregion

        #region ContextTools

        public static CVRM_Context ContextBuilder
        {
            get
            {
                return new CVRM_Context();
            }
        }

        private CVRM_Context context;
        public CVRM_Context Context
        {
            get { if (context == null) context = ContextBuilder; return context; }
            set { context = value; }
        }

        #endregion

        private static string tmpPath;
        /// <summary>
        /// Used to save the Temp files path(where we save pictures and other file to display it)
        /// </summary>
        public static string TempPath
        {
            get
            {
                if (string.IsNullOrEmpty(tmpPath))
                {
                    //Full Temp Directory
                    tmpPath = Path.GetTempPath() + "CVRM/";
                }
                return tmpPath;
            }
        }

        private static User currentUser;
        public static User CurrentUser
        {
            get { return currentUser; }
            set { currentUser = value;/* if (value != null) Communication.CurrentUserID = value.ID;*/ }
        }



        private static Language currentLanguage=Language.Arabic;
        public static Language CurrentLanguage
        {
            get { return currentLanguage; }
            set
            {
                currentLanguage = value;
                //Reach the app.xaml File
                var appFile = (App)Application.Current;
                ResourceDictionary languageDict = new ResourceDictionary();
                //Construct the current Language file Path
                string uri = "..\\Res\\LangRes\\" + CurrentLanguage.ToString() + "\\Lang.xaml";
                languageDict.Source = new Uri(uri, UriKind.Relative);
                //get all MergedDictionaries inside App.xaml file
                var mergedDictionaries = appFile.Resources.MergedDictionaries;
                //Find and delete any previous resources Dictionary
                var deleted = mergedDictionaries.Where(c => c.Source.ToString().Contains("Lang")).FirstOrDefault();
                mergedDictionaries.Remove(deleted);
                //Add the new Language File
                mergedDictionaries.Add(languageDict);
            }
        }

        #region DisplayFields(DisplayTools)

        private static TextBlock loadingTextBlock;

        public static TextBlock LoadingTextBlock
        {
            get { return loadingTextBlock; }
            set { loadingTextBlock = value; }
        }

        private string loadingMessage;

        public string LoadingMessage
        {
            get { return loadingMessage; }
            set
            {
                loadingMessage = value;
                if (LoadingTextBlock != null) Dispatcher.Invoke(() => { LoadingTextBlock.Text = value; });
                OnPropertyChanged();
            }
        }

        public static MaterialDesignThemes.Wpf.DialogHost LoadingPanel { get; set; }

        private static List<UIElement> navigationPanels;

        public static List<UIElement> NavigationControls
        {
            get
            {
                if (navigationPanels == null) navigationPanels = new List<UIElement>();
                return navigationPanels;
            }
            set { navigationPanels = value; }
        }
        private static TabControl tabsPanel;

        public static TabControl TabsPanel
        {
            get { return tabsPanel; }
            set { tabsPanel = value; }
        }

        public static TabItem CurrTab { get; set; }
        private TabItem lastTab;

        public TabItem LastTab
        {
            get { return lastTab; }
            set { lastTab = value; }
        }
        #endregion

        #endregion

        #region Constructors
        /// <summary>
        /// Determine the Type of Language and Create the Main Usercontrols
        /// </summary>
        static BaseVM()
        {
           // CurrentLanguage = Language.Arabic;
            //mainUC.Add(typeof(GUIs.Appointments.ViewAppointment).Name, new GUIs.Appointments.ViewAppointment());
            //mainUC.Add(typeof(GUIs.Messages.ViewMessages).Name, new GUIs.Messages.ViewMessages());
            //mainUC.Add(typeof(GUIs.Treatments.ViewTreatments).Name, new GUIs.Treatments.ViewTreatments());
            //mainUC.Add(typeof(GUIs.Accounts.Patients.ViewPatients).Name, new GUIs.Accounts.Patients.ViewPatients());
            //mainUC.Add(typeof(GUIs.Accounts.Doctors.ViewDoctors).Name, new GUIs.Accounts.Doctors.ViewDoctors());
            //mainUC.Add(typeof(GUIs.Payments.ViewPayments).Name, new GUIs.Payments.ViewPayments());

        }

        #endregion

        #region Commands
        private ObservableCollection<CommandVM> cmdLst;
        public ObservableCollection<CommandVM> CmdLst
        {
            get
            {
                if (cmdLst == null) cmdLst = CreateCommands();
                return cmdLst;
            }
        }
        /// <summary>
        /// Create the All Commands and return an ObservableCollection contains them
        /// Exception if the method hasn't overrided yet
        /// </summary>
        /// <returns>ObservableCollection contains the Command</returns>
        public virtual ObservableCollection<CommandVM> CreateCommands()
        {
            throw new NotImplementedException("Override create commands method in your VM");
        }


        #endregion

        #region Methods

        #region DisplayTools

        #region Loading
        public void startLoading(bool disableNavigationPanel=false)
        {
            if (LoadingPanel != null) LoadingPanel.IsOpen = true;
            if (disableNavigationPanel) disableNavigation();
        }
        public void stopLoading()
        {
            if (LoadingPanel != null) LoadingPanel.IsOpen = false;
        }
        public void enableNavigation()
        {
            foreach (var item in NavigationControls)
            {
                item.IsEnabled = true;
            }
        }
        public void disableNavigation()
        {
            foreach (var item in NavigationControls)
            {
                item.IsEnabled = false;
            }
        }
        #endregion

        /// <summary>
        /// This method is used to navigate between sub-usercontrols
        /// Generic to make creation and passing parameters easy
        /// </summary>
        /// <typeparam name="T">BaseUC Type</typeparam>
        /// <param name="createInstanceFunc">Create the UC with convenient parameters () => new dialog(params)</param>
        public void ViewUC<T>(Func<T> createInstanceFunc) where T : BaseUC
        {
            //Create the new Usercontrol
            AddTabItem(createInstanceFunc());
        }


        /// <summary>
        /// Add the UC to hisotry list
        /// </summary>
        void AddTabItem(BaseUC viewedItem)
        {
            //Clear the view
            // ViewPanel.Children.Clear();
            //Display the usercontrol on the MainWindow
            viewedItem.ViewModel.LastTab = CurrTab;
            TabItem newTab = new TabItem { Header = viewedItem.Title, Content = viewedItem};
            TabsPanel.Items.Add(newTab);
            //Refresh the usercontrol coomponets
            newTab.IsSelected = true;
            viewedItem.Refresh();
            CurrTab = newTab;
        }

        /// <summary>
        /// Close Current Usercontrol and open the pervious one 
        /// </summary>
        /// <param name="refresh">
        /// indicates that if the pervious view need to be refreshed or not
        /// </param>
        public void closeTab(bool refresh = false)
        {
            TabsPanel.Items.RemoveAt(TabsPanel.SelectedIndex);
            if (LastTab != null)
            {
                LastTab.IsSelected = true;

                if (refresh && LastTab.Content is BaseUC)
                {
                    ((BaseUC)LastTab.Content).RemoveData();
                    ((BaseUC)LastTab.Content).Refresh();
                }
                CurrTab = LastTab;
            }
        }
               

        #endregion

        #region DataTools
        #region DataTools
        /// <summary>
        /// This method is being call to remove the datalist from the View-model
        /// Stages:
        /// 1-set all the Datalist (which contains data) to null in order to deallocate them
        /// </summary>
        public abstract void removeData();
      
        #endregion

        #region Entities Tools
        public void addEntity(object entity)
        {
            Context.Entry(entity).State = System.Data.Entity.EntityState.Added;
            Context.SaveChanges();
        }
        public void refreshEntity(object entity)
        {
            Context.Entry(entity).Reload();
        }
        public void deleteEntity(object entity)
        {
            Context.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
            Context.SaveChanges();
        }
        public void modifiyEntity(object entity)
        {
            Context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            Context.SaveChanges();
        }
        #endregion

        #endregion

        #endregion

        #region interface

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        internal void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        #region IData
        public abstract void getData();

        public void getDarta()
        {
            throw new NotImplementedException();
        }
        #endregion

        #endregion

    }
    public abstract class BaseVM<T> : BaseVM
    {

    }
}