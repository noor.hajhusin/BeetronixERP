﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace CVRM.VMs.Employee
{
    public class IndexVM : BaseVM
    {

        #region cotr
        public IndexVM()
        {

        }
        #endregion

        #region properties
        public List<CVRM.Model.Employee> FilteredEmployees
        {
            get
            {
                if (Searching) return Employees.Where(c => c.FirstName.Contains(SearchText)).ToList();
                else { return Employees.ToList(); }
            }
        }
        public ObservableCollection<CVRM.Model.Employee> employees;

        public ObservableCollection<CVRM.Model.Employee> Employees
        {
            get { return employees; }
            set
            {
                employees = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredEmployees");
            }
        }

        private object selectedEmployee;

        public object SelectedEmployee
        {
            get { return selectedEmployee; }
            set
            {
                selectedEmployee = value;
                OnPropertyChanged("SelectedEmployee");
            }
        }
        private bool searching;

        public bool Searching
        {
            get { return searching; }
            set
            {
                searching = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredEmployees");
            }
        }
        private string searchText;

        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
                OnPropertyChanged();
            }
        }


        #endregion

        #region commands

        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>
            {
                //0-addNewEmployee
                new CommandVM(new RelayCommand (param=>addNewEmployee())),
                //1-update
                new CommandVM(new RelayCommand (param=>update())),
                //2-deleteSelected
                new CommandVM(new RelayCommand (param=>deleteSelected())),
                //3-search
                new CommandVM(new RelayCommand (param=>search())),
                //4-cancelCearch
                new CommandVM(new RelayCommand (param=>cancelSearch())),
                //5-displayEmployee
                new CommandVM(new RelayCommand (param=>displayEmployee(param))),
                //6-ShowDepartments
                new CommandVM(new RelayCommand (param=>showDepartments())),

            };
        }

        #endregion

        #region IData interface

        public override void getData()
        {
            Employees = new ObservableCollection<Model.Employee>(Context.Employees.Include(c => c.Department).AsEnumerable());
            OnPropertyChanged("FilteredEmployees");
        }

        public override void removeData()
        {
            Context = null;
           // Employees = null;
        }
        #endregion

        #region Methods

        private void search()
        {
            if (SearchText != null && SearchText.Length > 0) Searching = true;
        }
        void addNewEmployee()
        {
            ViewUC(() => new Views.Employee.AddEdit()
            {
                ID = $"Employee0",
                Title = $"New Employee"
            });
        }
        private void cancelSearch()
        {
            SearchText = "";
            Searching = false;
        }
        void update()
        {
            if (SelectedEmployee == null) return;
            ViewUC(() => new Views.Employee.AddEdit(SelectedEmployee as Model.Employee)
            {
              
                ID = $"Employee{((Model.Employee)selectedEmployee).ID}",
                Title = $"Employee {((Model.Employee)selectedEmployee).FirstName}"
            });
        }
        void deleteSelected()
        {
            if (SelectedEmployee == null) return;

        }

        private void displayEmployee(object page)
        {
            if (SelectedEmployee == null) return;
            ViewUC(() => new Views.Employee.AddEdit(SelectedEmployee as Model.Employee)
            {
                ID = $"Employee{((Model.Employee)selectedEmployee).ID}",
                Title = $"Employee {((Model.Employee)selectedEmployee).FirstName}"
            });
        }
        void showDepartments()
        {
            ViewUC(() => new Views.Department.Index()
            {
                ID = $"Department",
                Title = $"Departments"
            });
        }

        #endregion
    }
}

