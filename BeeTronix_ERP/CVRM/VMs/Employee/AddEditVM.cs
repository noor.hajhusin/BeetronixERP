﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.VMs.Employee
{
   public class AddEditVM : BaseVM
    {
        #region cotr
        public AddEditVM()
        {
            Employee = new Model.Employee() { };

        }
        public AddEditVM(Model.Employee employeee)
        {
            this.Employee = Context.Employees.Find(employeee.ID);
        }

        #endregion
        #region properties
        private Model.Employee employee;
        public Model.Employee Employee
        {
            private set
            {
                employee = value;
                OnPropertyChanged("Employee");
            }
            get
            {
                return employee;
            }
        }

        private Model.Department department;
        public Model.Department Department
        {
            private set
            {
                department = value;
                OnPropertyChanged("Department");
            }
            get
            {
                return department;
            }
        }
        public ObservableCollection<Model.Department> Departments { get; set; }
        #endregion

        #region Commands
        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>
            {
                //0-save&Close
                new CommandVM(new RelayCommand (param=>saveClose())),
                //1-cancel
                new CommandVM(new RelayCommand (param=>cancel())),


            };
        }
        #endregion
        #region Methods

        async void saveClose()
        {
            LoadingMessage = "Saving ...";
            startLoading();
            await Task.Run(() =>
            {
                if (Employee.ID > 0) modifiyEntity(Employee);
                else addEntity(Employee);

            });
            stopLoading();
            closeTab(true);
        }

        void cancel()
        {
            closeTab(false);
        }

        public override void removeData()
        {
          
        }

        public override void getData()
        {
            Departments = new ObservableCollection<Model.Department>(Context.Departments.AsEnumerable());
        }
        #endregion
    }
}
