﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.VMs.Product
{
    public class AddEditVM : BaseVM
    {
        
        #region cotr
        public AddEditVM()
        {
            Product = new Model.Product() { PublishDate=DateTime.Today};
         
        }
        public AddEditVM(Model.Product Product)
        {
            this.Product = Context.Products.Find(Product.ID);
        }

        #endregion

        #region Properties
        private Model.Product product;
        public Model.Product Product
        {
            private set
            {
                product = value;
                OnPropertyChanged("Product");
            }
            get
            {
                return product;
            }
        }


        private Model.ProductType productType;
        public Model.ProductType ProductType
        {
            private set
            {
                productType = value;
                OnPropertyChanged("ProductType");
            }
            get
            {
                return productType;
            }
        }

        public ObservableCollection<Model.ProductType> ProductTypes { get; set; }
        public ObservableCollection<Model.ProductStatus> ProductStatuses
        {
            get {
                return new ObservableCollection<Model.ProductStatus>(Enum.GetValues(typeof(Model.ProductStatus)).Cast<Model.ProductStatus>());
                }
        }
        #endregion Properties

        #region Commands
        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>
            {
                //0-save&Close
                new CommandVM(new RelayCommand (param=>saveClose())),
                //1-cancel
                new CommandVM(new RelayCommand (param=>cancel())),
             

            };
        }
        #endregion
        #region Methods

        async void saveClose()
        {
            LoadingMessage = "Saving ...";
            startLoading();
            await Task.Run(() =>
            {
                if (Product.ID > 0) modifiyEntity(Product);
                else addEntity(Product);
               
            });
            stopLoading();
            closeTab(true);
        }
    
        void cancel()
        {
            closeTab(false);
        }
        #endregion


        public override void getData()
        {

            ProductTypes = new ObservableCollection<Model.ProductType>(Context.ProductTypes.AsEnumerable());
        }

        public override void removeData()
        {
          
        }
    }
}
