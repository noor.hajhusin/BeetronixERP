﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CVRM.VMs.Product
{
    public class IndexVM : BaseVM
    {
        #region ctor

        public IndexVM()
        {

        }
        #endregion

        #region Properties

        public List<CVRM.Model.Product> FilteredProducts
        {
            get
            {
                if (Searching) return Products.Where(c => c.Name.Contains(SearchText)).ToList();
                else { return Products.ToList(); }
            }
        }

        public ObservableCollection<CVRM.Model.Product> products;
        public ObservableCollection<CVRM.Model.Product> Products
        {
            get { return products; }
            set
            {
                products = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredProducts");
            }
        }
        private object selectedProduct;

        public object SelectedProduct
        {
            get { return selectedProduct; }
            set
            {
                selectedProduct = value;
                OnPropertyChanged("SelectedProduct");
            }
        }

        private bool searching;

        public bool Searching
        {
            get { return searching; }
            set
            {
                searching = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredProducts");
            }
        }
        private string searchText;

        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
                OnPropertyChanged();
            }
        }

     


        #endregion

        #region commands

        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>
            {
                //0-addNewProduct
                new CommandVM(new RelayCommand (param=>addNewProduct())),
                //1-update
                new CommandVM(new RelayCommand (param=>update())),
                //2-deleteSelected
                new CommandVM(new RelayCommand (param=>deleteSelected())),
                //3-search
                new CommandVM(new RelayCommand (param=>search())),
                //4-cancelCearch
                new CommandVM(new RelayCommand (param=>cancelSearch())),
                //5-displayProduct
                new CommandVM(new RelayCommand (param=>displayProduct(param))),
            };
        }

        #endregion

        #region IData interface
        public override void getData()
        {
            Products = new ObservableCollection<Model.Product>(Context.Products.Include(c => c.ProductType).AsEnumerable());
            OnPropertyChanged("FilteredProducts");
        }

        public override void removeData()
        {
            Context = null;
            //Products = null;
        }
        #endregion

        #region Methods
     
        private void search()
        {
            if (SearchText != null && SearchText.Length > 0) Searching = true;
        }
        void addNewProduct()
        {
            ViewUC(() => new Views.Product.AddEdit()
            {
                ID = $"Product0",
                Title = $"New Product"
            });
        }
        private void cancelSearch()
        {
            SearchText = "";
            Searching = false;
        }
        void update()
        {
            if (SelectedProduct == null) return;
            ViewUC(() => new Views.Product.AddEdit(SelectedProduct as Model.Product)
            {
                ID = $"Product{((Model.Product)selectedProduct).ID}",
                Title = $"Product {((Model.Product)selectedProduct).Name}"
            });
        }
        void deleteSelected()
        {
            if (SelectedProduct == null) return;

        }

        private void displayProduct(object page)
        {
            //if (SelectedProduct == null) return;
            //ViewUC(() => new Views.Products.AddEdit(SelectedProduct as Model.Product, OpenMode.Modify, int.Parse(page.ToString()))
            //{
            //    ID = $"Product{((Model.Product)selectedProduct).ID}",
            //    Title = $"Product {((Model.Product)selectedProduct).Name}"
            //});
        }
        #endregion

    }
}
