﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.VMs.Department
{
    public class IndexVM : BaseVM
    {

        #region cotr
        public IndexVM()
        {

        }
        #endregion
        #region properties
        public List<CVRM.Model.Department> FilteredDepartments
        {
            get
            {
                if (Searching) return Departments.Where(c => c.Name.Contains(SearchText)).ToList();
                else { return Departments.ToList(); }
            }
        }
        public ObservableCollection<CVRM.Model.Department> departments;

        public ObservableCollection<CVRM.Model.Department> Departments
        {
            get { return departments; }
            set
            {
                departments = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredDepartmens");
            }
        }
        private object selectedDepartment;

        public object SelectedDepartment
        {
            get { return selectedDepartment; }
            set
            {
                selectedDepartment = value;
                OnPropertyChanged("SelectedDepartment");
            }
        }
        private bool searching;

        public bool Searching
        {
            get { return searching; }
            set
            {
                searching = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredDepartments");
            }
        }
        private string searchText;

        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
                OnPropertyChanged();
            }
        }

        #endregion
        #region commands

        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>
            {
                //0-addNewDepartment
                new CommandVM(new RelayCommand (param=>addNewDepartment())),
                //1-update
                new CommandVM(new RelayCommand (param=>update())),
                //2-deleteSelected
                new CommandVM(new RelayCommand (param=>deleteSelected())),
                //3-search
                new CommandVM(new RelayCommand (param=>search())),
                //4-cancelCearch
                new CommandVM(new RelayCommand (param=>cancelSearch())),
                //5-displayDepartment
                new CommandVM(new RelayCommand (param=>displayDepartment(param))),

            };
        }

        #endregion

        public override void getData()
        {
            Departments = new ObservableCollection<Model.Department>(Context.Departments.AsEnumerable());

            OnPropertyChanged("FilteredDepartments");
        }
    

        public override void removeData()
        {
            Context = null;
        }


        #region Methods

        private void search()
        {
            if (SearchText != null && SearchText.Length > 0) Searching = true;
        }
        void addNewDepartment()
        {
            ViewUC(() => new Views.Department.AddEdit()
            {
                ID = $"Department0",
                Title = $"Department"
            });
        }
        private void cancelSearch()
        {
            SearchText = "";
            Searching = false;
        }
        void update()
        {
            if (SelectedDepartment == null) return;
            ViewUC(() => new Views.Department.AddEdit(SelectedDepartment as Model.Department)
            {

                ID = $"Department{((Model.Department)SelectedDepartment).ID}",
                Title = $"Department {((Model.Department)SelectedDepartment).Name}"
            });
        }
        void deleteSelected()
        {
            if (SelectedDepartment == null) return;

        }

        private void displayDepartment(object page)
        {
            if (SelectedDepartment == null) return;
            ViewUC(() => new Views.Employee.AddEdit(SelectedDepartment as Model.Employee)
            {
                ID = $"Department{((Model.Department)SelectedDepartment).ID}",
                Title = $"Department {((Model.Department)selectedDepartment).Name}"
            });
        }


        #endregion
    }
}
