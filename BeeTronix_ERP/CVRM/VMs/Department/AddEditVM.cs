﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.VMs.Department
{
    public class AddEditVM : BaseVM
    {

        #region cotr
        public AddEditVM()
        {
            Department = new Model.Department() { };

        }
        public AddEditVM(Model.Department department)
        {
            this.Department = Context.Departments.Find(department.ID);
        }
        #endregion

        #region properties 
        private Model.Department department;
        public Model.Department Department
        {
            private set
            {
                department = value;
                OnPropertyChanged("Department");
            }
            get
            {
                return department;
            }
        }




        #endregion

        #region Commands
        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>
            {
                //0-save&Close
                new CommandVM(new RelayCommand (param=>saveClose())),
                //1-cancel
                new CommandVM(new RelayCommand (param=>cancel())),


            };
        }
        #endregion

        #region Methods

        async void saveClose()
        {
            LoadingMessage = "Saving ...";
            startLoading();
            await Task.Run(() =>
            {
                if (Department.ID > 0) modifiyEntity(Department);
                else addEntity(Department);

            });
            stopLoading();
            closeTab(true);
        }

        void cancel()
        {
            closeTab(false);
        }

        public override void getData()
        {
          
        }

        public override void removeData()
        {
         
        }
        #endregion
    }
}
