﻿using CVRM.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace CVRM.VMs
{
    public class LoginWindowVM : BaseVM<LoginWindowVM>
    {
        private double myVar;


        public double MyProperty
        {
            get { return myVar; }
            set { myVar = value;OnPropertyChanged(); }
        }

        #region Fields
        // 0 showAllUsers - 1 showUser - 
        private int page;

        public int Page
        {
            get { return page; }
            set { page = value; OnPropertyChanged(); }
        }
        private bool keepLoggedIn;

        public bool KeepLoggedIn
        {
            get { return keepLoggedIn; }
            set { keepLoggedIn = value; OnPropertyChanged(); }
        }
        private User lastLoggedInUser;

        public User LastLoggedInUser
        {
            get { return lastLoggedInUser; }
            set { lastLoggedInUser = value; OnPropertyChanged(); }
        }


        #region GUIs

        #region DependencyProperties

        #endregion

        #endregion

        #region Filtered Lists

        #region Search Strings
        #endregion

        #region Lists
        #endregion

        #endregion

        #region Data Lists
        ObservableCollection<User> users;
        public ObservableCollection<User> Users
        {
            get { return users; }
            set
            {
                users = value;
                OnPropertyChanged();
            }
        }
         
        #endregion

        #region Selected Data
        #endregion




        #endregion

        #region Commands

        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>()
            {
                //0-login
                new CommandVM(new RelayCommand(c=>login())),
                //0-cancel
                new CommandVM(new RelayCommand(c=>cancel())),
            };
        }
        #endregion

        #region Constructors
        
        public LoginWindowVM()
        {
            MyProperty = 2000000.42134;
        }
        #endregion

        #region Methods

        public override void getData()
        {
            Users = new ObservableCollection<User>(Context.Users.AsEnumerable());
            object uID= Context.Settings.Where(c => c.Key == "CurrentUserID").First().Details;
            int id = 0;
            if (uID == null||!int.TryParse(uID.ToString(),out id))
            {
                
            }
            else
            {
               LastLoggedInUser = Users.Where(c => c.ID == int.Parse(uID.ToString())).FirstOrDefault();
                LoadingMessage = $"Logging in {LastLoggedInUser.Name}";
                System.Threading.Thread.Sleep(3000);
                CurrentUser = LastLoggedInUser;
            }

        }

        public override void removeData()
        {

        }

         void login()
        {

        }
        void cancel()
        {

        }

        #endregion
    }
}