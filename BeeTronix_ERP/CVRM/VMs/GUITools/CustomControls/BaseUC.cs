﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace CVRM.VMs.GUITools.CustomControls
{
    public abstract class BaseUC : UserControl, IController
    {
        #region Fields

        #region Abstract Properties
        public abstract BaseVM ViewModel { get; set; }
        #endregion
        #region DependencyProperties
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static readonly DependencyProperty TitleProperty;

        public bool NeedScrollBar
        {
            get { return (bool)GetValue(NeedScrollBarProperty); }
            set { SetValue(NeedScrollBarProperty, value); }
        }

        public static readonly DependencyProperty NeedScrollBarProperty;
        public string ID
        {
            get { return (string)GetValue(IDProperty); }
            set { SetValue(IDProperty, value); }
        }
        public static readonly DependencyProperty IDProperty;



        #endregion
        #endregion

        #region Constructors
        static BaseUC()
        {
            TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(BaseUC), new PropertyMetadata(default(string)));
            NeedScrollBarProperty = DependencyProperty.Register("NeedScrollBar", typeof(bool), typeof(BaseUC), new PropertyMetadata(true));
            IDProperty = DependencyProperty.Register("IDProperty", typeof(string), typeof(BaseUC), new PropertyMetadata(default(string)));
            DefaultStyleKeyProperty.OverrideMetadata(typeof(BaseUC), new FrameworkPropertyMetadata(typeof(BaseUC)));
        }

        #endregion

        #region Interface (Abstract Implement)

        #region IController
        /// <summary>
        /// Close the VM and remove the data from it 
        /// </summary>
        public virtual void RemoveData()
        {
            //Safty condition (in case that the view Does not has a View model)
            if (ViewModel == null)
            {
                return;
            }
            //Remove the Data from the VM
            ViewModel.removeData();
        }

        /// <summary>
        /// Call the GetData from the VM to grap the data from DataBase with Loading progress
        /// </summary>
        public async virtual void Refresh()
        {
            this.Visibility = Visibility.Collapsed;
            ViewModel.LoadingMessage = "Loading";
            ViewModel.startLoading(true);
            DataContext = null;
            await Task.Run(() =>
            {
                ViewModel.getData();
            });
            this.DataContext = ViewModel;
            this.Visibility = Visibility.Visible;
            ViewModel.stopLoading();
            ViewModel.enableNavigation();
        }

        #endregion

        #endregion
    }
}
