﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace CVRM.VMs.GUITools.CustomControls
{
   public class Dialog : BaseUC
    {
        public override BaseVM ViewModel
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        #region Ctors
        public Dialog()
        {

        }
        static Dialog()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Dialog), new FrameworkPropertyMetadata(typeof(Dialog)));

        }
        #endregion

        #region Commands
        public ICommand SaveCommand
        {
            get { return (ICommand)GetValue(SaveCommandProperty); }
            set { SetValue(SaveCommandProperty, value); }
        }

        public static readonly DependencyProperty SaveCommandProperty =
            DependencyProperty.Register("SaveCommand", typeof(ICommand), typeof(Dialog), new PropertyMetadata(null));

        public ICommand CancelCommand
        {
            get { return (ICommand)GetValue(CancelCommandProperty); }
            set { SetValue(CancelCommandProperty, value); }
        }

        public static readonly DependencyProperty CancelCommandProperty =
            DependencyProperty.Register("CancelCommand", typeof(ICommand), typeof(Dialog), new PropertyMetadata(null));

        public object CancelCommandParameter
        {
            get { return (object)GetValue(CancelCommandParameterProperty); }
            set { SetValue(CancelCommandParameterProperty, value); }
        }

        public static readonly DependencyProperty CancelCommandParameterProperty =
            DependencyProperty.Register("CancelCommandParameter", typeof(object), typeof(Dialog), new PropertyMetadata(default(object)));

        public object SaveCommandParameter
        {
            get { return (object)GetValue(SaveCommandParameterProperty); }
            set { SetValue(SaveCommandParameterProperty, value); }
        }

        public static readonly DependencyProperty SaveCommandParameterProperty =
            DependencyProperty.Register("SaveCommandParameter", typeof(object), typeof(Dialog), new PropertyMetadata(default(object)));

        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(Dialog), new PropertyMetadata(default(CornerRadius)));


        #endregion

        #region MyRegion
        public void CloseWithSave()
        {
            var command = SaveCommand;
            if (command != null && command.CanExecute(SaveCommandParameter))
                command.Execute(SaveCommandParameter);

        }
        public void Close()
        {
            var command = CancelCommand;
            if (command != null && command.CanExecute(CancelCommandParameter))
                command.Execute(CancelCommandParameter);
        }
        #endregion
    }
}
