﻿using System;

namespace CVRM.VMs.GUITools.CustomControls
{
    public interface IController
    {
        BaseVM ViewModel { get; set; }
        void Refresh();
        void RemoveData();
    }
}
