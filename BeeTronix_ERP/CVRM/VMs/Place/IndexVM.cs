﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.VMs.Place
{
    public class IndexVM : BaseVM
    {
        #region Properties

        public List<CVRM.Model.Place> FilteredPlaces
        {
            get
            {
                if (Searching) return Places.Where(c => c.Name.Contains(SearchText)).ToList();
                else { return Places.ToList(); }
            }
        }

        public ObservableCollection<CVRM.Model.Place> places;
        public ObservableCollection<CVRM.Model.Place> Places
        {
            get { return places; }
            set
            {
                places = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredPlaces");
            }
        }
        private object selectedPlace;

        public object SelectedPlace
        {
            get { return selectedPlace; }
            set
            {
                selectedPlace = value;
                OnPropertyChanged("SelectedPlace");
            }
        }

        private bool searching;

        public bool Searching
        {
            get { return searching; }
            set
            {
                searching = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredPlaces");
            }
        }
        private string searchText;

        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
                OnPropertyChanged();
            }
        }


        #endregion

        #region AddEditPlaceDialog

        #region properties
        private Model.Place placeDialogObject;
        public Model.Place PlaceDialogObject
        {
            private set
            {
                placeDialogObject = value;
                OnPropertyChanged("PlaceDialogObject");
            }
            get
            {
                return placeDialogObject;
            }
        }

        private bool isAddPlaceDialogOpened;

        public bool IsAddPlaceDialogOpened
        {
            get { return isAddPlaceDialogOpened; }
            set
            {
                isAddPlaceDialogOpened = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Methods

        async void saveClose()
        {
            LoadingMessage = "Saving ...";
            startLoading();
            await Task.Run(() =>
            {
                if (PlaceDialogObject.ID > 0) modifiyEntity(PlaceDialogObject);
                else
                {
                    addEntity(PlaceDialogObject);
                    Places.Add(placeDialogObject);
                    OnPropertyChanged("FilteredPlaces");
                }

            });
            stopLoading();
            IsAddPlaceDialogOpened = false;
            PlaceDialogObject = null;
        }

        void cancel()
        {
            IsAddPlaceDialogOpened = false;
            PlaceDialogObject = null;
        }
        #endregion


        #endregion

        #region commands
        
        public ObservableCollection<CommandVM> AddEditCommands { get; set; }

        public override ObservableCollection<CommandVM> CreateCommands()
        {

            AddEditCommands = new ObservableCollection<CommandVM>
            {
                
                //0-save&Close
                new CommandVM(new RelayCommand (param=>saveClose())),
                //1-cancel
                new CommandVM(new RelayCommand (param=>cancel())),
            };

            return new ObservableCollection<CommandVM>
            {
                
                //0-addNewPlaceDialog
                new CommandVM(new RelayCommand (param=>addEditPlaceDialog())),
                //1-update
                new CommandVM(new RelayCommand (param=>update())),
                //2-deleteSelected
                new CommandVM(new RelayCommand (param=>deleteSelected())),
                //3-search
                new CommandVM(new RelayCommand (param=>search())),
                //4-cancelCearch
                new CommandVM(new RelayCommand (param=>cancelSearch())),

            };

        }


        #endregion

        #region IData interface
        public override void getData()
        {
            Places = new ObservableCollection<Model.Place>(Context.Places.AsEnumerable());
            OnPropertyChanged("FilteredPlaces");
        }

        public override void removeData()
        {
            Context = null;
            
        }
        #endregion

        #region Methods

        private void search()
        {
            if (SearchText != null && SearchText.Length > 0) Searching = true;
        }
        void addEditPlaceDialog()
        {
            PlaceDialogObject = new Model.Place();
            IsAddPlaceDialogOpened = true;


        }
        private void cancelSearch()
        {
            SearchText = "";
            Searching = false;
        }
        void update()
        {
            PlaceDialogObject = (Model.Place)selectedPlace;
            IsAddPlaceDialogOpened = true;
        }
        void deleteSelected()
        {
            if (SelectedPlace == null) return;

        }

       
        #endregion

    }
}
