﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVRM.VMs.DialogTools
{
    public interface IDialogService
    {
        Task<MessageDialogResult> AskQuestionAsync(string title, string message , MessageDialogStyle messageDialogStylee = MessageDialogStyle.AffirmativeAndNegative, MetroDialogSettings messageSetting=null);
        Task<ProgressDialogController> ShowProgressAsync(string title, string message);
        Task ShowMessageAsync(string title, string message);
        Task<string> ShowInputAsync(string title, string message, MessageDialogStyle messageDialogStype = MessageDialogStyle.AffirmativeAndNegative, MetroDialogSettings messageSettings = null);
    }

    public class DialogService : IDialogService
    {
        private readonly MetroWindow metroWindow;

        public DialogService(MetroWindow metroWindow)
        {
            this.metroWindow = metroWindow;
        }

        public Task<MessageDialogResult> AskQuestionAsync(string title, string message,MessageDialogStyle messageDialogStyle=MessageDialogStyle.AffirmativeAndNegative, MetroDialogSettings messageSetting=null)
        {
            MetroDialogSettings messageDialogSetting = messageSetting;


            if (messageDialogSetting == null)
            {
                messageDialogSetting = new MetroDialogSettings()
                {
                    AffirmativeButtonText = "ok",
                    NegativeButtonText = "cancel",
                    ColorScheme = MetroDialogColorScheme.Accented
                };
               
            }
            return metroWindow.ShowMessageAsync(title, message,
               messageDialogStyle, messageDialogSetting);

        }

        public Task<ProgressDialogController> ShowProgressAsync(string title, string message)
        {
            return metroWindow.ShowProgressAsync(title, message);
        }

        public Task ShowMessageAsync(string title, string message)
        {
            return metroWindow.ShowMessageAsync(title, message);
        }

        public Task<string> ShowInputAsync(string title, string message, MessageDialogStyle messageDialogStype = MessageDialogStyle.AffirmativeAndNegative, MetroDialogSettings messageSettings = null)
        {
            return metroWindow.ShowInputAsync(title, message, messageSettings);
        }
    }
}
