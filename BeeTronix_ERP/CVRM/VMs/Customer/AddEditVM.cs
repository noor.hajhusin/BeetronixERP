﻿using CVRM.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using MaterialDesignThemes.Wpf;
using System.Windows.Data;
using System.Globalization;
using System.Data.Entity;


public class CallDuration
{
    public TimeSpan Duration { get; set; }
    public override string ToString()
    {
        return $" {Duration.Hours.ToString("##")} : {Duration.Minutes.ToString("##")} Hour(s)";
    }
}
public enum OpenMode { Add,Modify,Display}

namespace CVRM.VMs.Customer
{

    public class AddEditVM : BaseVM
    {
        #region ctor

        public AddEditVM()
        {
            Customer = new Model.Customer();
            this.OpenMode = OpenMode.Add;
            Page = 0;
        }
        public AddEditVM(Model.Customer Customer, OpenMode OpenMode, int Page)
        {
            this.Customer = Context.Customers.Find(Customer.ID);
            this.OpenMode = OpenMode;
            this.Page = Page;
        }
        #endregion

        #region Properties
        private OpenMode openMode;

        public OpenMode OpenMode
        {
            get { return openMode; }
            set { openMode = value; OnPropertyChanged(); }
        }

        #region Data Lists
        private ObservableCollection<CVRM.Model.Product> products;

        public ObservableCollection<CVRM.Model.Product> Products
        {
            get { return products; }
            set { products = value; OnPropertyChanged(); }
        }
        private ObservableCollection<CVRM.Model.CustomerProduct> customerProducts;

        public ObservableCollection<CVRM.Model.CustomerProduct> CustomerProducts
        {
            get { return customerProducts; }
            set
            {
                customerProducts = value;
                OnPropertyChanged();
            }
        }
        private ObservableCollection<CVRM.Model.Person> people;

        public ObservableCollection<CVRM.Model.Person> People
        {
            get { return people; }
            set { people = value; OnPropertyChanged(); }
        }
        private ObservableCollection<CVRM.Model.Activity> activities;

        public ObservableCollection<CVRM.Model.Activity> Activities
        {
            get { return activities; }
            set { activities = value; OnPropertyChanged(); }
        }
        private ObservableCollection<CVRM.Model.Detail> details;

        public ObservableCollection<CVRM.Model.Detail> Details
        {
            get { return details; }
            set { details = value; OnPropertyChanged(); }
        }
        private ObservableCollection<CVRM.Model.CustomerPerson> customerRelatedPeople;

        public ObservableCollection<CVRM.Model.CustomerPerson> CustomerRelatedPeople
        {
            get { return customerRelatedPeople; }
            set
            {
                customerRelatedPeople = value;
                OnPropertyChanged();
            }
        }
        private ObservableCollection<CVRM.Model.CustomerActivity> customerActivities;

        public ObservableCollection<CVRM.Model.CustomerActivity> CustomerActivities
        {
            get { return customerActivities; }
            set
            {
                customerActivities = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<CVRM.Model.CustomerDetail> customerDetails;

        public ObservableCollection<CVRM.Model.CustomerDetail> CustomerDetails
        {
            get { return customerDetails; }
            set
            {
                customerDetails = value; OnPropertyChanged();
            }
        }
        private ObservableCollection<CVRM.Model.CustomerFile> customerFiles;

        public ObservableCollection<CVRM.Model.CustomerFile> CustomerFiles
        {
            get { return customerFiles; }
            set
            {
                customerFiles = value;
                OnPropertyChanged("FilteredFiles");
            }
        }
        private ObservableCollection<CVRM.Model.Employee> employees;

        public ObservableCollection<CVRM.Model.Employee> Employees
        {
            get { return employees; }
            set
            {
                employees = value;
                OnPropertyChanged();
            }
        }
        private ObservableCollection<CVRM.Model.Place> places;

        public ObservableCollection<CVRM.Model.Place> Places
        {
            get { return places; }
            set
            {
                places = value;
                OnPropertyChanged();
            }
        }
        private ObservableCollection<CVRM.Model.Call> customerCalls;

        public ObservableCollection<CVRM.Model.Call> CustomerCalls
        {
            get { return customerCalls; }
            set
            {
                customerCalls = value;
                OnPropertyChanged("FilteredCalls");
            }
        }
        private ObservableCollection<CVRM.Model.Call> customerReminders;

        public ObservableCollection<CVRM.Model.Call> CustomerReminders
        {
            get { return customerReminders; }
            set
            {
                customerReminders = value;
                OnPropertyChanged("FilteredReminders");
            }
        }

        public ObservableCollection<CVRM.Model.CallType> CallTypes
        {
            get { return new ObservableCollection<CallType> { CallType.Incoming, CallType.Outgoing, CallType.Missed, CallType.Meeting, CallType.Visit, CallType.Support ,CallType.Social }; }
        }
        #region FilteredData
        public List<Call> FilteredCalls
        {
            get
            {
                var t = CustomerCalls.AsEnumerable();
                if (SearchCall != null && SearchCall.Length > 0) t = t.Where(c => c.Details.Contains(SearchCall));
                if (CallTypeFilter != null && (CallTypeFilter is CallType)) t = t.Where(c => c.CallType == (CallType)CallTypeFilter);
                return t.OrderBy(c => c.Date).OrderBy(c => c.Time).ToList();
            }
        }
        public List<Call> FilteredReminders
        {
            get
            {
                var t = CustomerReminders.AsEnumerable();
                if (SearchReminder != null && SearchReminder.Length > 0) t = t.Where(c => c.Details.Contains(SearchReminder));
                return t.OrderBy(c => c.Date).OrderBy(c => c.Time).ToList();
            }
        }
        public List<Model.CustomerFile> FilteredFiles
        {
            get
            {
                var t = CustomerFiles.AsEnumerable();
                if (SearchCustomerFile != null && SearchCustomerFile.Length > 0) t = t.Where(c => c.File.Name.Contains(SearchCustomerFile));
                if (CustomerFileTypeFilter != null) t = t.Where(c => c.CustomerFileType.Name == ((CustomerFileType)CustomerFileTypeFilter).Name);
                return t.ToList();
            }
        }
        #endregion
        #region deleted Data
        /*
                private ObservableCollection<CVRM.Model.Person> deletedPeople;

                public ObservableCollection<CVRM.Model.Person> DeletedPeople
                {
                    get { return deletedPeople; }
                    set { deletedPeople = value; OnPropertyChanged(); }
                }
                private ObservableCollection<CVRM.Model.Activity> deletedActivities;

                public ObservableCollection<CVRM.Model.Activity> DeletedActivities
                {
                    get { return deletedActivities; }
                    set { deletedActivities = value; OnPropertyChanged(); }
                }
                private ObservableCollection<CVRM.Model.Detail> deletedDetails;

                public ObservableCollection<CVRM.Model.Detail> DeletedDetails
                {
                    get { return deletedDetails; }
                    set { deletedDetails = value; OnPropertyChanged(); }
                }
                private ObservableCollection<CVRM.Model.CustomerFile> deletedFiles;

                public ObservableCollection<CVRM.Model.CustomerFile> DeletedFiles
                {
                    get { return deletedFiles; }
                    set
                    {
                        deletedFiles = value;
                    }
                }
                private ObservableCollection<CVRM.Model.Call> deletedCalls;

                public ObservableCollection<CVRM.Model.Call> DeletedCalls
                {
                    get { return deletedCalls; }
                    set
                    {
                        deletedCalls = value;
                    }
                }*/
        #endregion
        #endregion
        #region Data Objects
        private Model.Customer customer;
        public Model.Customer Customer
        {
            get { return customer; }
            set { customer = value; OnPropertyChanged(); }
        }
        private object selectedPerson;

        public object SelectedPerson
        {
            get { return selectedPerson; }
            set
            {
                selectedPerson = value;
                OnPropertyChanged();
            }
        }
        private CallPerson selectedCallPerson;

        public CallPerson SelectedCallPerson
        {
            get { return selectedCallPerson; }
            set
            {
                selectedCallPerson = value;
                OnPropertyChanged();
            }
        }
        private CallPlace selectedCallPlace;

        public CallPlace SelectedCallPlace
        {
            get { return selectedCallPlace; }
            set
            {
                selectedCallPlace = value;
                OnPropertyChanged();
            }
        }
        private CallEmployee selectedCallEmployee;

        public CallEmployee SelectedCallEmployee
        {
            get { return selectedCallEmployee; }
            set
            {
                selectedCallEmployee = value;
                OnPropertyChanged();
            }
        }
        private CustomerPerson newCallPerson;

        public CustomerPerson NewCallPerson
        {
            get { return newCallPerson; }
            set
            {
                newCallPerson = value;
                OnPropertyChanged();
            }
        }
        private Model.Place newCallPlace;

        public Model.Place NewCallPlace
        {
            get { return newCallPlace; }
            set
            {
                newCallPlace = value;
                OnPropertyChanged();
            }
        }
        private Model.Employee newCallEmployee;

        public Model.Employee NewCallEmployee
        {
            get { return newCallEmployee; }
            set
            {
                newCallEmployee = value;
                OnPropertyChanged();
            }
        }
        private object selectedProduct;

        public object SelectedProduct
        {
            get { return selectedProduct; }
            set
            {
                selectedProduct = value;
                OnPropertyChanged();
            }
        }
        private object selectedActivity;

        public object SelectedActivity
        {
            get { return selectedActivity; }
            set
            {
                selectedActivity = value;
                OnPropertyChanged();
            }
        }
        private object selectedDetail;

        public object SelectedDetail
        {
            get { return selectedDetail; }
            set
            {
                selectedDetail = value;
                if (selectedDetail == null) { CustomerDetail = null; EnableDetailValue = false; }
                else
                {
                    EnableDetailValue = true;
                    CustomerDetail = new CustomerDetail { Customer = Customer, Detail = selectedDetail as Detail };
                }
                OnPropertyChanged();
            }
        }
        private bool enableDetailValue;

        public bool EnableDetailValue
        {
            get { return enableDetailValue; }
            set { enableDetailValue = value; OnPropertyChanged(); }
        }

        private CustomerDetail customerDetail;

        public CustomerDetail CustomerDetail
        {
            get { return customerDetail; }
            set
            {
                customerDetail = value;
                OnPropertyChanged();
            }
        }
        private object deletedObject;

        public object DeletedObject
        {
            get { return deletedObject; }
            set
            {
                deletedObject = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Dialogs

        #region AddActivityDialog 
        private ObservableCollection<CallPerson> callPeople;

        public ObservableCollection<CallPerson> CallPeople
        {
            get { return callPeople; }
            set { callPeople = value;
                OnPropertyChanged();
            }
        }
        private ObservableCollection<CallEmployee> callEmployees;

        public ObservableCollection<CallEmployee> CallEmployees
        {
            get { return callEmployees; }
            set
            {
                callEmployees = value;
                OnPropertyChanged();
            }
        }
        private ObservableCollection<CVRM.Model.CallPlace> callPlaces;

        public ObservableCollection<CVRM.Model.CallPlace> CallPlaces
        {
            get { return callPlaces; }
            set
            {
                callPlaces = value;
                OnPropertyChanged();
            }
        }

        private string newActivityName;

        public string NewActivityName
        {
            get { return newActivityName; }
            set
            {
                newActivityName = value;
                OnPropertyChanged();
            }
        }
        private bool isAddActivityDialogOpened;

        public bool IsAddActivityDialogOpened
        {
            get { return isAddActivityDialogOpened; }
            set
            {
                isAddActivityDialogOpened = value;
                OnPropertyChanged();
            }
        }

        private Activity activityDialogObject;

        public Activity ActivityDialogObject
        {
            get { return activityDialogObject; }
            set
            {
                activityDialogObject = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region AddPersonDialog 
        public ObservableCollection<Job> Jobs { get; set; }

        ObservableCollection<PersonDetail> newPersonDetails;
        public ObservableCollection<PersonDetail> NewPersonDetails
        {
            get { return newPersonDetails; }
            set
            {
                newPersonDetails = value;
                OnPropertyChanged();
            }
        }

        private bool isAddPersonDialogOpened;

        public bool IsAddPersonDialogOpened
        {
            get { return isAddPersonDialogOpened; }
            set
            {
                isAddPersonDialogOpened = value;
                OnPropertyChanged();
            }
        }
        private PersonDetail newPersonDetail;

        public PersonDetail NewPersonDetail
        {
            get { return newPersonDetail; }
            set
            {
                newPersonDetail = value;
                OnPropertyChanged();
                OnPropertyChanged("NewPersonDetails");
            }
        }
        private string newJobName;

        public string NewJobName
        {
            get { return newJobName; }
            set
            {
                newJobName = value;
                OnPropertyChanged();
            }
        }

        private Model.CustomerPerson personDialogObject;

        public Model.CustomerPerson PersonDialogObject
        {
            get { return personDialogObject; }
            set
            {
                personDialogObject = value;
                OnPropertyChanged();
            }
        }
        private object selectedCustomerPerson;

        public object SelectedCustomerPerson
        {
            get { return selectedCustomerPerson; }
            set
            {
                selectedCustomerPerson = value;
                OnPropertyChanged();
            }
        }
        private object selectedPersonDetail;

        public object SelectedPersonDetail
        {
            get { return selectedPersonDetail; }
            set
            {
                selectedPersonDetail = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region AddDetailDialog 

        private bool isAddDetailDialogOpened;

        public bool IsAddDetailDialogOpened
        {
            get { return isAddDetailDialogOpened; }
            set
            {
                isAddDetailDialogOpened = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region AddProductDialog 

        private string newProductName;

        public string NewProductName
        {
            get { return newProductName; }
            set
            {
                newProductName = value;
                OnPropertyChanged();
            }
        }

        private string newProductTypeName;

        public string NewProductTypeName
        {
            get { return newProductTypeName; }
            set
            {
                newProductTypeName = value;
                OnPropertyChanged();
            }
        }
        private bool isProductDialogOpened;

        public bool IsProductDialogOpened
        {
            get { return isProductDialogOpened; }
            set
            {
                isProductDialogOpened = value;
                OnPropertyChanged();
            }
        }

        private Model.Product productDialogObject;

        public Model.Product ProductDialogObject
        {
            get { return productDialogObject; }
            set
            {
                productDialogObject = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<ProductType> productTypes;
        public ObservableCollection<ProductType> ProductTypes
        {
            get { return productTypes; }
            set
            {
                productTypes = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region ConfirmDialog 

        private bool isConfirmDialogOpened;

        public bool IsConfirmDialogOpened
        {
            get { return isConfirmDialogOpened; }
            set
            {
                isConfirmDialogOpened = value;
                OnPropertyChanged();
            }
        }

        private string confirmDialogMessage;

        public string ConfirmDialogMessage
        {
            get { return confirmDialogMessage; }
            set
            {
                confirmDialogMessage = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region CustomerFileDialog
        public ObservableCollection<CustomerFileType> CustomerFileTypes
        {
            get { return new ObservableCollection<CustomerFileType>(Context.CustomerFileTypes.AsEnumerable()); }
        }
        private ObservableCollection<CVRM.Model.FileType> fileTypes;

        public ObservableCollection<CVRM.Model.FileType> FileTypes
        {
            get { return fileTypes; }
            set { fileTypes = value; OnPropertyChanged(); }
        }
        private object selectedCustomerFile;

        public object SelectedCustomerFile
        {
            get { return selectedCustomerFile; }
            set
            {
                selectedCustomerFile = value;
                OnPropertyChanged();
            }
        }
        private bool isAddFileDialogOpened;

        public bool IsAddFileDialogOpened
        {
            get { return isAddFileDialogOpened; }
            set
            {
                isAddFileDialogOpened = value;
                OnPropertyChanged();
            }
        }
        private bool saveFileData;

        public bool SaveFileData
        {
            get { return saveFileData; }
            set
            {
                saveFileData = value;
                OnPropertyChanged();
            }
        }

        private object customerFileTypeFilter;

        public object CustomerFileTypeFilter
        {
            get { return customerFileTypeFilter; }
            set
            {
                customerFileTypeFilter = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredFiles");
            }
        }

        private string searchCustomerFile;

        public string SearchCustomerFile
        {
            get { return searchCustomerFile; }
            set
            {
                searchCustomerFile = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredFiles");
            }
        }
        private Model.CustomerFile fileDialogObject;

        public Model.CustomerFile FileDialogObject
        {
            get { return fileDialogObject; }
            set
            {
                fileDialogObject = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region CallDialog
        private ObservableCollection<TimeSpan> callDurations;

        public ObservableCollection<TimeSpan> CallDurations
        {
            get
            {
                if (callDurations == null) callDurations = new ObservableCollection<TimeSpan>();
                for (int i = 0; i <= 480; i += 30)
                {
                    callDurations.Add(TimeSpan.FromMinutes(i));
                }
                return callDurations;
            }
            set { callDurations = value; OnPropertyChanged(); }
        }


        private bool isCallDialogOpened;

        public bool IsCallDialogOpened
        {
            get { return isCallDialogOpened; }
            set
            {
                isCallDialogOpened = value;
                OnPropertyChanged();
            }
        }
        public bool AddNewReminder { get; set; }
        private bool isAddReminderDialogOpened;

        public bool IsAddReminderDialogOpened
        {
            get { return isAddReminderDialogOpened; }
            set
            {
                isAddReminderDialogOpened = value;
                OnPropertyChanged();
            }
        }

        private object callFileTypeFilter;

        public object CallTypeFilter
        {
            get { return callFileTypeFilter; }
            set
            {
                callFileTypeFilter = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredCalls");
            }
        }
        private object selectedCustomerCall;

        public object SelectedCustomerCall
        {
            get { return selectedCustomerCall; }
            set
            {
                selectedCustomerCall = value;
                OnPropertyChanged();
            }
        }
        private object selectedCustomerReminder;

        public object SelectedCustomerReminder
        {
            get { return selectedCustomerReminder; }
            set
            {
                selectedCustomerReminder = value;
                OnPropertyChanged();
            }
        }
        private object selectedCustomerProduct;

        public object SelectedCustomerProduct
        {
            get { return selectedCustomerProduct; }
            set
            {
                selectedCustomerProduct = value;
                OnPropertyChanged();
            }
        }
        private object selectedCustomerDetail;

        public object SelectedCustomerDetail
        {
            get { return selectedCustomerDetail; }
            set
            {
                selectedCustomerDetail = value;
                OnPropertyChanged();
            }
        }
        private object selectedCustomerActivity;

        public object SelectedCustomerActivity
        {
            get { return selectedCustomerActivity; }
            set
            {
                selectedCustomerActivity = value;
                OnPropertyChanged();
            }
        }

        private string searchCall;

        public string SearchCall
        {
            get { return searchCall; }
            set
            {
                searchCall = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredCalls");
            }
        }
        private string searchReminder;

        public string SearchReminder
        {
            get { return searchReminder; }
            set
            {
                searchReminder = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredReminders");
            }
        }
        private Call callDialogObject;

        public Call CallDialogObject
        {
            get { return callDialogObject; }
            set
            {
                callDialogObject = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #endregion
        private int page;

        public int Page
        {
            get { return page; }
            set { page = value; OnPropertyChanged(); }
        }

        #endregion

        #region commands
        public ObservableCollection<CommandVM> PersonDialogCommands { get; set; }
        public ObservableCollection<CommandVM> ProductDialogCommands { get; set; }
        public ObservableCollection<CommandVM> DetailDialogCommands { get; set; }
        public ObservableCollection<CommandVM> ActivityDialogCommands { get; set; }
        public ObservableCollection<CommandVM> JobDialogCommands { get; set; }
        public ObservableCollection<CommandVM> CallDialogCommands { get; set; }
        public ObservableCollection<CommandVM> FileDialogCommands { get; set; }


        public override ObservableCollection<CommandVM> CreateCommands()
        {
            ActivityDialogCommands = new ObservableCollection<CommandVM>
            {
                //0-activityDialogAccept
                new CommandVM(new RelayCommand(param=>activityDialogAccept())),
                //0-activityDialogCancel
                new CommandVM(new RelayCommand(param=>activityDialogCancel())),
            };
            CallDialogCommands = new ObservableCollection<CommandVM>
            {
                //0-callDialogAccept
                new CommandVM(new RelayCommand(param=>callDialogAccept())),
                //1-callDialogCancel
                new CommandVM(new RelayCommand(param=>callDialogCancel())),
                //2-addCallEmployee
                new CommandVM(new RelayCommand(param=>addCallEmployee())),
                //3-addCallPerson
                new CommandVM(new RelayCommand(param=>addCallPerson())),
                //4-delCallEmployee
                new CommandVM(new RelayCommand(param=>delCallEmployee())),
                //5-delCallPerson
                new CommandVM(new RelayCommand(param=>delCallPerson())),
                //6-reminderDialogAccept
                new CommandVM(new RelayCommand(param=>reminderDialogAccept())),
                //7-reminderDialogCancel
                new CommandVM(new RelayCommand(param=>reminderDialogCancel())),
                //8-addCallPerson
                new CommandVM(new RelayCommand(param=>addCallPlace())),
                //9-delCallEmployee
                new CommandVM(new RelayCommand(param=>delCallPlace())),
            };
            PersonDialogCommands = new ObservableCollection<CommandVM>
            {
                //0-personDialogAccept
                new CommandVM(new RelayCommand(param=>personDialogAccept())),
                //1-personDialogCancel
                new CommandVM(new RelayCommand(param=>personDialogCancel())),
                //2-addPersonDetail
                new CommandVM(new RelayCommand(param=>addPersonDetail())),
                //3-deletePersonDetail
                new CommandVM(new RelayCommand(param=>deletePersonDetail())),
            };
            FileDialogCommands = new ObservableCollection<CommandVM>
            {
                //0-customerFileDialogAccept
                new CommandVM(new RelayCommand(param=>customerFileDialogAccept())),
                //0-customerFileDialogCancel
                new CommandVM(new RelayCommand(param=>customerFileDialogCancel())),
            };
            ProductDialogCommands = new ObservableCollection<CommandVM>
            {
                //0-productDialogAccept
                new CommandVM(new RelayCommand(param=>productDialogAccept())),
                //0-productDialogCancel
                new CommandVM(new RelayCommand(param=>productDialogCancel())),
            };
            return new ObservableCollection<CommandVM>
            {
                //0-Save
                new CommandVM(new RelayCommand (param=>save())),
                //1-cancel
                new CommandVM(new RelayCommand (param=>cancel())),
                //2-add activity
                new CommandVM(new RelayCommand (param=>addActivity())),
                //3-addProduct
                new CommandVM(new RelayCommand (param=>addProduct())),
                //4-add detail
                new CommandVM(new RelayCommand (param=>addDetail())),
                //5-addNewCustomerReminder
                new CommandVM(new RelayCommand (param=>addNewCustomerReminder())),
                //6-markCallAsDone
                new CommandVM(new RelayCommand (param=>markCallAsDone(param))),
                //7-
                new CommandVM(new RelayCommand (param=>doNothing())),// replaced
                //8-addNewActivity
                new CommandVM(new RelayCommand (param=>addNewActivity())),
                //9-addNewProduct
                new CommandVM(new RelayCommand (param=>addNewProduct())),
                //10-
                new CommandVM(new RelayCommand (param=>doNothing())),// replaced
                //11-addNewPerson
                new CommandVM(new RelayCommand (param=>addNewPerson())),
                //12-exportFile
                new CommandVM(new RelayCommand (param=>exportFile())),
                //13-changePage
                new CommandVM(new RelayCommand (param=>changePage(param))),
                //14-openFile
                new CommandVM(new RelayCommand (param=>openFile())),
                //15-
                new CommandVM(new RelayCommand (param=>doNothing())),// replaced
                //16-
                new CommandVM(new RelayCommand (param=>doNothing())),// replaced
                //17-
                new CommandVM(new RelayCommand (param=>doNothing())),// replaced
                //18-
                new CommandVM(new RelayCommand (param=>doNothing())),// replaced
                //19-
                new CommandVM(new RelayCommand (param=>doNothing())),// replaced
                //20-confirmDialogYes
                new CommandVM(new RelayCommand (param=>confirmDialogYes())),
                //21-confirmDialogNo
                new CommandVM(new RelayCommand (param=>confirmDialogNo())),
                //22-addNewCustomerFile
                new CommandVM(new RelayCommand (param=>addNewCustomerFile())),
                //23-
                new CommandVM(new RelayCommand (param=>doNothing())),// replaced
                //24-
                new CommandVM(new RelayCommand (param=>doNothing())),// replaced
                //25-browseFile
                new CommandVM(new RelayCommand (param=>browseFile())),
                //26-addNewCustomerCall
                new CommandVM(new RelayCommand (param=>addNewCustomerCall())),
                //27-
                new CommandVM(new RelayCommand (param=>doNothing())),// replaced
                //28-
                new CommandVM(new RelayCommand (param=>doNothing())),// replaced
                //29-dGridItemModify
                new CommandVM(new RelayCommand (param=>dGridItemModify(param))),
                //30-dGridItemDelete
                new CommandVM(new RelayCommand (param=>dGridItemDelete(param)))
            };
        }

        private CommandVM Authorise(int v)
        {
            throw new NotImplementedException();
        }




        #endregion

        #region IData interface
        public override void getData()
        {
            //Customer Lists
            if (Customer.ID > 0)
            {
                CustomerFiles = new ObservableCollection<Model.CustomerFile>(Context.CustomerFiles.Where(c => c.CustomerID == Customer.ID).OrderBy(c=>c.CreationDate).AsEnumerable());
                CustomerActivities = new ObservableCollection<Model.CustomerActivity>(Context.CustomerActivities.Where(c => c.CustomerID == Customer.ID).AsEnumerable());
                CustomerDetails = new ObservableCollection<Model.CustomerDetail>(Context.CustomerDetails.Where(c => c.CustomerID == Customer.ID).AsEnumerable());
                CustomerRelatedPeople = new ObservableCollection<Model.CustomerPerson>(Context.CustomerPeople.Where(c => c.CustomerID == Customer.ID)
                    .Include(c => c.Person.PersonDetails.Select(x => x.Detail)).AsEnumerable());
                var lst = Context.Calls.Where(c => c.CustomerID == Customer.ID).Include(c=>c.CallPlaces).ToList();
                CustomerCalls = new ObservableCollection<Call>(lst.Where(c => c.CustomerID == Customer.ID&&c.CallType!=CallType.Reminder).AsEnumerable());
                CustomerReminders = new ObservableCollection<Call>(lst.Where(c => c.CustomerID == Customer.ID&&c.CallType==CallType.Reminder).AsEnumerable());
                CustomerProducts = new ObservableCollection<CustomerProduct>(Context.CustomerProducts.Where(c => c.CustomerID == Customer.ID).AsEnumerable());
            }
            else
            {
                CustomerFiles = new ObservableCollection<Model.CustomerFile>();
                CustomerActivities = new ObservableCollection<Model.CustomerActivity>();
                CustomerDetails = new ObservableCollection<Model.CustomerDetail>();
                CustomerRelatedPeople = new ObservableCollection<Model.CustomerPerson>();
                CustomerCalls = new ObservableCollection<Call>();
                CustomerReminders = new ObservableCollection<Call>();
                CustomerProducts = new ObservableCollection<CustomerProduct>();
            }
            //get 
            People = new ObservableCollection<Model.Person>(Context.People.AsEnumerable());
            Details = new ObservableCollection<Model.Detail>(Context.Details.AsEnumerable());
            Activities = new ObservableCollection<Model.Activity>(Context.Activities.AsEnumerable());
            Products = new ObservableCollection<Model.Product>(Context.Products.AsEnumerable());
            ProductTypes = new ObservableCollection<Model.ProductType>(Context.ProductTypes.AsEnumerable());

            //filter data lists
            foreach (var item in CustomerRelatedPeople)
            {
                if (People.Contains(item.Person)) People.Remove(item.Person);
            }
        }

        public override void removeData()
        {
            Context = null;
        }
        #endregion

        #region Methods
        void doNothing() { }
        #region Navigation
        async void save()
        {
            LoadingMessage = "Saving ...";
            startLoading();
            await Task.Run(() =>
            {
                if (customer.ID > 0) modifiyEntity(customer);
                else addEntity(customer);
                foreach (var item in CustomerActivities)
                {
                    if (item.ID > 0) modifiyEntity(item);
                    else addEntity(item);
                }
                foreach (var item in CustomerDetails)
                {
                    if (item.ID > 0) modifiyEntity(item);
                    else addEntity(item);
                }
                foreach (var item in CustomerProducts)
                {
                    if (item.ID > 0) modifiyEntity(item);
                    else addEntity(item);
                }
            });
            stopLoading();
            closeTab(true);
        }

        private void cancel()
        {
            closeTab();
        }

        private void changePage(object page)
        {
            Page = int.Parse(page.ToString());
        }
        #endregion
        #region add
        private void addActivity()
        {
            Activity a = SelectedActivity as Activity;
            if (a == null)
            {
                if (NewActivityName==null|| NewActivityName.Length == 0) return;
                else a = new Activity { Name = NewActivityName };
            }
            CustomerActivity ca = new CustomerActivity { Customer = Customer, Activity = a };
            CustomerActivities.Add(ca);
            if (SelectedActivity != null) Activities.Remove(SelectedActivity as Activity);
            NewActivityName = null;
        }
        void addProduct()
        {
            Model.Product newP = SelectedProduct as Model.Product;
            if (newP == null)
            {
                if (NewProductName == null || NewProductName.Length == 0) return;
                else newP = new Model.Product { Name = NewProductName };
            }
            CustomerProduct cp = new CustomerProduct { Product = newP, Customer = Customer };
            Products.Remove(newP);
            CustomerProducts.Add(cp);
            NewProductName = "";
        }
        void addDetail()
        {
            if (CustomerDetail == null) return;
            if (!CustomerDetails.Contains(CustomerDetail)) CustomerDetails.Add(CustomerDetail);
            //Details.Remove(SelectedDetail as Detail);
            SelectedDetail = null;
        }
        private void addNewActivity()
        {
            ActivityDialogObject = new Activity();
            IsAddActivityDialogOpened = true;
        }
        private void addNewProduct()
        {
            ProductDialogObject = new Model.Product();
            IsProductDialogOpened = true;
        }

        private void addNewPerson()
        {
            Jobs = new ObservableCollection<Job>(Context.Jobs.AsEnumerable());
            OnPropertyChanged("Jobs");
            PersonDialogObject = new CustomerPerson { Person = new Model.Person(), Customer = Customer };
            NewPersonDetails = new ObservableCollection<PersonDetail>();
            NewPersonDetail = new PersonDetail { Person = PersonDialogObject.Person };
            IsAddPersonDialogOpened = true;
        }
        #endregion
        #region Dialogs
        private void activityDialogAccept()
        {
            // if (!CustomerActivities.Contains(ActivityDialogObject)) CustomerActivities.Add(ActivityDialogObject);
            if (ActivityDialogObject.ID > 0) modifiyEntity(ActivityDialogObject);
            else addEntity(ActivityDialogObject);
            IsAddActivityDialogOpened = false;
        }
        private void activityDialogCancel()
        {
            ActivityDialogObject = null;
            IsAddActivityDialogOpened = false;
        }
        private void personDialogAccept()
        {
            if (PersonDialogObject.Job == null && NewJobName.Length > 0) PersonDialogObject.Job = new Job { Name = NewJobName };
            PersonDialogObject.Person.PersonDetails = NewPersonDetails;
            if (!CustomerRelatedPeople.Contains(PersonDialogObject)) CustomerRelatedPeople.Add(PersonDialogObject);

            OnPropertyChanged("CustomerRelatedPeople");
            NewPersonDetail = null;
            if (PersonDialogObject.ID > 0) modifiyEntity(PersonDialogObject);
            else addEntity(PersonDialogObject);
            IsAddPersonDialogOpened = false;
        }
        private void personDialogCancel()
        {
            ActivityDialogObject = null;
            NewPersonDetail = null;
            Jobs = null;
            IsAddPersonDialogOpened = false;
        }
        private void productDialogAccept()
        {
            if (ProductDialogObject.ProductType == null && NewProductTypeName.Length > 0)
                ProductDialogObject.ProductType = new ProductType { Name = NewProductTypeName };
            //  if (!Products.Contains(ProductDialogObject)) Products.Add(ProductDialogObject);
            //  CustomerProduct cp = new CustomerProduct { Customer = Customer, Product = ProductDialogObject };
            //  if(!CustomerProducts.Contains(cp)) CustomerProducts.Add(cp);

            if (ProductDialogObject.ID > 0) modifiyEntity(ProductDialogObject);
            else addEntity(ProductDialogObject);
            IsProductDialogOpened = false;
        }
        private void productDialogCancel()
        {
            ProductDialogObject = null;
            IsProductDialogOpened = false;
        }
        private void confirmDialogYes()
        {
            if (DeletedObject is CustomerDetail)
            {
                CustomerDetail cd = DeletedObject as CustomerDetail;
                if (cd.ID > 0) deleteEntity(cd);
                CustomerDetails.Remove(cd);
                // Details.Add(cd.Detail);
            }
            else if (DeletedObject is Model.CustomerPerson)
            {
                Model.CustomerPerson p = DeletedObject as Model.CustomerPerson;
                if (p.ID > 0) deleteEntity(p);
                CustomerRelatedPeople.Remove(p);
                // CustomerRelatedPeople.Add(p);
            }
            if (DeletedObject is CustomerActivity)
            {
                CustomerActivity ca = DeletedObject as CustomerActivity;
                if (ca.ID > 0) deleteEntity(ca);
                CustomerActivities.Remove(ca);
                Activities.Add(ca.Activity);
            }
            if (DeletedObject is Model.CustomerFile)
            {
                Model.CustomerFile cf = DeletedObject as Model.CustomerFile;
                if (cf.ID > 0) deleteEntity(cf);
                CustomerFiles.Remove(cf);
                OnPropertyChanged("FilteredFiles");
            }
            if (DeletedObject is Call)
            {
                Call call = DeletedObject as Call;
                if (call.ID > 0) deleteEntity(call);
                CustomerCalls.Remove(call);
                OnPropertyChanged("FilteredCalls");
            }
            IsConfirmDialogOpened = false;
            DeletedObject = null;
        }

        private void confirmDialogNo()
        {
            IsConfirmDialogOpened = false;
            DeletedObject = null;
        }
        #endregion
        private void addNewCustomerFile()
        {
            FileTypes = new ObservableCollection<FileType>(Context.FileTypes.AsEnumerable());
            FileDialogObject = new Model.CustomerFile { Customer = Customer, Datetime = DateTime.Now };
            SaveFileData = true;
            OnPropertyChanged("SaveFileData");
            IsAddFileDialogOpened = true;
        }
        private void customerFileDialogAccept()
        {
            if (SaveFileData)
                if (FileDialogObject.File.Data == null) FileDialogObject.File.Data = new byte[] { };
            if (!CustomerFiles.Contains(FileDialogObject)) CustomerFiles.Add(FileDialogObject);
            OnPropertyChanged("FilteredFiles");
            if (FileDialogObject.ID > 0)
            {
                int index = CustomerFiles.IndexOf(SelectedCustomerFile as Model.CustomerFile);
                CustomerFiles.Remove(SelectedCustomerFile as Model.CustomerFile);
                CustomerFiles.Insert(index, FileDialogObject);
                modifiyEntity(FileDialogObject);
            }
            else
            {
                addEntity(FileDialogObject);
                CustomerFiles.Add(FileDialogObject);
            }
            IsAddFileDialogOpened = false;
        }
        private void customerFileDialogCancel()
        {
            FileDialogObject = null;
            IsAddFileDialogOpened = false;
        }

        private async void browseFile()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Multiselect = false;
            if (dlg.ShowDialog().Value)
            {
                startLoading();
                await Task.Run(() =>
                {
                    string v = dlg.FileName;
                    FileDialogObject.File = new Model.File { Name = System.IO.Path.GetFileNameWithoutExtension(v), Path = v };
                    System.IO.FileStream fs = new System.IO.FileStream(v, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    FileDialogObject.File.Data = new byte[fs.Length];
                    fs.Read(FileDialogObject.File.Data, 0, System.Convert.ToInt32(fs.Length));
                    fs.Close();
                    string ext = System.IO.Path.GetExtension(v).Remove(0, 1);
                    FileType ft = FileTypes.Where(c => c.Extension.ToUpper().Contains(ext.ToUpper())).FirstOrDefault();
                    FileDialogObject.File.FileType = ft == null ? FileDialogObject.File.FileType : ft;
                    OnPropertyChanged("FileDialogObject");
                });
                stopLoading();
            }
        }
        private async void exportFile()
        {
            Model.CustomerFile cf = SelectedCustomerFile as Model.CustomerFile;
            if (cf.File == null) { return; }
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.InitialDirectory = System.IO.Path.GetDirectoryName(cf.File.Path);
            dlg.FileName = cf.File.Name;
            string ext = System.IO.Path.GetExtension(cf.File.Path).Remove(0, 1);
            string type = cf.File.FileType == null ? "Unknown" : cf.File.FileType.Name;
            string filter = $@"{type} Files (*.{ext})|*.{ext}|All Files (*.*)|*.*";
            dlg.Filter = filter;
            if (dlg.ShowDialog().Value)
            {
                LoadingMessage = "Export File ...";
                startLoading();
                await Task.Run(() =>
                {
                    string v = dlg.FileName;
                    System.IO.FileStream fs = new System.IO.FileStream(v, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite);
                    fs.Write(cf.File.Data, 0, System.Convert.ToInt32(cf.File.Data.Count()));
                    fs.Close();
                });
                stopLoading();
            }
        }
        private async void openFile()
        {
            Model.CustomerFile cf = SelectedCustomerFile as Model.CustomerFile;
            if (cf.File == null) { return; }
            LoadingMessage = "Opening File ...";
            startLoading();
            string tempFileName = System.IO.Path.GetTempFileName();
            string fileName = System.IO.Path.GetDirectoryName(tempFileName) + System.IO.Path.GetFileName(cf.File.Path);
            await Task.Run(() =>
            {
                System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite);
                fs.Write(cf.File.Data, 0, System.Convert.ToInt32(cf.File.Data.Count()));
                fs.Close();
            });
            System.Diagnostics.Process.Start(fileName);
            stopLoading();
        }

        private void addNewCustomerCall()
        {
            Employees = new ObservableCollection<Model.Employee>(Context.Employees.AsEnumerable());
            Places = new ObservableCollection<Model.Place>(Context.Places.AsEnumerable());
            CallPlaces = new ObservableCollection<CallPlace>();
            CallPeople = new ObservableCollection<CallPerson>();
            CallEmployees = new ObservableCollection<CallEmployee>();
            CallDialogObject = new Call { Customer = Customer, Date = DateTime.Today, Time = DateTime.Now };
            IsCallDialogOpened = true;
        }
        private void callDialogAccept()
        {
            CallDialogObject.CallEmployees = CallEmployees;
            CallDialogObject.CallPeople = CallPeople;
            CallDialogObject.CallPlaces = CallPlaces;
            if (AddNewReminder)
            {
                Call newReminder = new Call { CallType = CallType.Reminder, Reminder = CallDialogObject };
                CustomerReminders.Add(newReminder);
            }
            if (CallDialogObject.ID > 0)
            {
                int index = CustomerCalls.IndexOf(SelectedCustomerCall as Call);
                CustomerCalls.Remove(SelectedCustomerCall as Call);
                CustomerCalls.Insert(index, CallDialogObject);
                modifiyEntity(CallDialogObject);
            }
            else
            {
                addEntity(CallDialogObject);
                CustomerCalls.Add(CallDialogObject);
            }
            OnPropertyChanged("CallDialogObject");
            OnPropertyChanged("FilteredCalls");
            IsCallDialogOpened = false;
        }

        private void callDialogCancel()
        {
            if (CallDialogObject.ID > 0) refreshEntity(CallDialogObject);
            CallDialogObject = null;
            IsCallDialogOpened = false;
        }
        void markCallAsDone(object param)
        {
            switch (param.ToString())
            {
                case "reminder":
                    (SelectedCustomerReminder as Call).IsClosed = true;
                    int index = CustomerReminders.IndexOf(SelectedCustomerReminder as Call);
                    CustomerReminders.Remove(SelectedCustomerReminder as Call);
                    CustomerReminders.Insert(index, SelectedCustomerReminder as Call);
                    OnPropertyChanged("FilteredReminders");
                    break;
                case "call":
                    (SelectedCustomerCall as Call).IsClosed = true;
                    index= CustomerCalls.IndexOf(SelectedCustomerCall as Call);
                    CustomerCalls.Remove(SelectedCustomerCall as Call);
                    CustomerCalls.Insert(index,SelectedCustomerCall as Call);
                    OnPropertyChanged("FilteredCalls");
                    break;
                default: break;
            }
        }
        private void dGridItemModify(object param)
        {
            switch (param.ToString())
            {
                case "activity":
                    ActivityDialogObject =Context.Activities.Find((SelectedCustomerActivity as CustomerActivity).Activity.ID);
                    IsAddActivityDialogOpened = true;
                    break;
                case "product":
                    ProductDialogObject = Context.Products.Find((SelectedCustomerProduct as CustomerProduct).Product.ID);
                    IsProductDialogOpened = true;
                    break;
                case "detail":
                    CustomerDetail = (SelectedCustomerDetail as CustomerDetail);
                    // IsCallDialogOpened = true;
                    break;
                case "call":
                    CallDialogObject = SelectedCustomerCall as Call;
                    Places = new ObservableCollection<Model.Place>(Context.Places.AsEnumerable());
                    CallPlaces = new ObservableCollection<CallPlace>(CallDialogObject.CallPlaces);
                    Employees = new ObservableCollection<Model.Employee>(Context.Employees.AsEnumerable());
                    CallPeople = new ObservableCollection<CallPerson>(CallDialogObject.CallPeople);
                    CallEmployees = new ObservableCollection<CallEmployee>(CallDialogObject.CallEmployees);
                    IsCallDialogOpened = true;
                    break;
                case "file":
                    FileTypes = new ObservableCollection<FileType>(Context.FileTypes.AsEnumerable());
                    FileDialogObject = Context.CustomerFiles.Find((SelectedCustomerFile as Model.CustomerFile).ID);
                    SaveFileData = true;
                    OnPropertyChanged("SaveFileData");
                    IsAddFileDialogOpened = true;
                    break;
                case "reminder":
                    CallDialogObject = SelectedCustomerReminder as Call;
                    IsAddReminderDialogOpened = true;
                    break;
                case "person":
                    PersonDialogObject = Context.CustomerPeople.Find((SelectedCustomerPerson as CustomerPerson).ID);
                    Jobs = new ObservableCollection<Job>(Context.Jobs.AsEnumerable());
                    OnPropertyChanged("Jobs");
                    NewPersonDetail = new PersonDetail { Person = PersonDialogObject.Person };
                    NewPersonDetails = new ObservableCollection<PersonDetail>(PersonDialogObject.Person.PersonDetails.AsEnumerable());
                    IsAddPersonDialogOpened = true;
                    break;
                default:
                    break;
            }
        }
        private void dGridItemDelete(object param)
        {
            switch (param.ToString())
            {
                case "call":
                    DeletedObject = SelectedCustomerCall;
                    ConfirmDialogMessage = $"Confirm delete  {(SelectedCustomerCall as Call).CallType}  ?";
                    break;
                case "file":
                    DeletedObject = SelectedCustomerFile;
                    ConfirmDialogMessage = $"Confirm delete file {(SelectedCustomerFile as Model.CustomerFile).File.Name}  ?";
                    break;
                case "person":
                    DeletedObject = SelectedCustomerPerson;
                    ConfirmDialogMessage = $"Confirm delete person {(SelectedCustomerPerson as CustomerPerson).Person.FullName}  ?";
                    break;
                case "activity":
                    DeletedObject = SelectedCustomerActivity;
                    ConfirmDialogMessage = $"Confirm delete activity {(SelectedCustomerActivity as CustomerActivity).Activity.Name}  ?";
                    break;
                case "detail":
                    DeletedObject = SelectedCustomerDetail;
                    ConfirmDialogMessage = $"Confirm delete detail {(SelectedCustomerDetail as CustomerDetail).Detail.Name}  ?";
                    break;
                case "reminder":
                    DeletedObject = SelectedCustomerReminder;
                    ConfirmDialogMessage = $"Confirm delete Reminder ?";
                    break;
                case "product":
                    DeletedObject = SelectedCustomerProduct;
                    ConfirmDialogMessage = $"Confirm delete product {(SelectedCustomerProduct as CustomerProduct).Product.Name}  ?";
                    break;
                default:
                    break;
            }
            IsConfirmDialogOpened = true;
        }

        private void addPersonDetail()
        {
            if (NewPersonDetail == null) { return; }
            NewPersonDetails.Add(NewPersonDetail);
            OnPropertyChanged("NewPersonDetails");
            NewPersonDetail = new PersonDetail { Person = PersonDialogObject.Person };
        }
        private void deletePersonDetail()
        {
            if (SelectedPersonDetail == null) return;
            if ((SelectedPersonDetail as PersonDetail).ID > 0) deleteEntity(SelectedPersonDetail);
            NewPersonDetails.Remove(SelectedPersonDetail as PersonDetail);
            OnPropertyChanged("NewPersonDetails");
        }
        private void addCallEmployee()
        {
            if (NewCallEmployee == null) return;
            CallEmployee ce = new CallEmployee { Call = CallDialogObject, Employee = NewCallEmployee };
            CallEmployees.Add(ce);
            OnPropertyChanged("CallEmployees");
            NewCallEmployee = null;
        }
        private void addCallPerson()
        {
            if (NewCallPerson == null) return;
            CallPerson cp = new CallPerson { Person = NewCallPerson.Person, Call = CallDialogObject };
            CallPeople.Add(cp);
            OnPropertyChanged("CallPeople");
            NewCallPerson = null;
        }
        private void delCallEmployee()
        {
            if (SelectedCallEmployee == null) return;
            if(SelectedCallEmployee.ID>0)
                deleteEntity(SelectedCallEmployee);
            CallEmployees.Remove(SelectedCallEmployee);
            OnPropertyChanged("CallEmployees");
        }

        private void delCallPerson()
        {
            if (SelectedCallPerson == null) return;
            if (SelectedCallPerson.ID > 0) deleteEntity(SelectedCallPerson);
            CallPeople.Remove(SelectedCallPerson);
            OnPropertyChanged("CallPeople");
        }
        private void addCallPlace()
        {
            if (NewCallPlace == null) return;
            CallPlace cp = new CallPlace { Place = NewCallPlace, Call = CallDialogObject };
            CallPlaces.Add(cp);
            OnPropertyChanged("CallPlaces");
            NewCallPlace = null;
        }
        private void delCallPlace()
        {
            if (SelectedCallPlace == null) return;
            if (SelectedCallPlace.ID > 0) deleteEntity(SelectedCallPlace);
            CallPlaces.Remove(SelectedCallPlace);
            OnPropertyChanged("CallPlaces");
        }

        private void addNewCustomerReminder()
        {
            CallDialogObject = new Call { Customer = Customer, Date = DateTime.Today, Time = DateTime.Now,CallType=CallType.Reminder };
            IsAddReminderDialogOpened = true;
        }
        private void reminderDialogAccept()
        {
            if (!CustomerCalls.Contains(CallDialogObject)) CustomerReminders.Add(CallDialogObject);
            OnPropertyChanged("CallDialogObject");
            OnPropertyChanged("FilteredReminders");
            if (CallDialogObject.ID > 0) modifiyEntity(CallDialogObject);
            else addEntity(CallDialogObject);
            IsAddReminderDialogOpened = false;
        }

        private void reminderDialogCancel()
        {
            if (CallDialogObject.ID > 0) refreshEntity(CallDialogObject);
            CallDialogObject = null;
            IsAddReminderDialogOpened = false;
        }

        #endregion
    }
}
