﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CVRM.VMs.Customer
{
    public class IndexVM : BaseVM
    {
        #region ctor

        public IndexVM()
        {

        }
        #endregion

        #region Properties

        public List<CVRM.Model.Customer> FilteredCustomers
        {
            get
            {
                if (Searching) return Customers.Where(c=>c.Name.Contains(SearchText)).ToList();
                else { return Customers.ToList();}
            }
        }

        public ObservableCollection<CVRM.Model.Customer> customers;
        public ObservableCollection<CVRM.Model.Customer> Customers
        {
            get { return customers; }
            set
            {
                customers = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredCustomers");
            }
        }
        private object selectedCustomer;

        public object SelectedCustomer
        {
            get { return selectedCustomer; }
            set
            {
                selectedCustomer = value;
                OnPropertyChanged("SelectedCustomer");
            }
        }

        private bool searching;

        public bool Searching
        {
            get { return searching; }
            set
            {
                searching = value;
                OnPropertyChanged();
                OnPropertyChanged("FilteredCustomers");
            }
        }
        private string searchText;

        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
                OnPropertyChanged();
            }
        }


        #endregion

        #region commands

        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>
            {
                //0-addNewCustomer
                new CommandVM(new RelayCommand (param=>addNewCustomer())),
                //1-update
                new CommandVM(new RelayCommand (param=>update())),
                //2-deleteSelected
                new CommandVM(new RelayCommand (param=>deleteSelected())),
                //3-search
                new CommandVM(new RelayCommand (param=>search())),
                //4-cancelCearch
                new CommandVM(new RelayCommand (param=>cancelSearch())),
                //5-displayCustomer
                new CommandVM(new RelayCommand (param=>displayCustomer(param))),
            };
        }

        #endregion

        #region IData interface
        public override void getData()
        {
            Customers=new ObservableCollection<Model.Customer>(Context.Customers.Include(c => c.CustomerActivities.Select(x => x.Activity))
                .Include(c => c.CustomerDetails.Select(x => x.Detail)).AsEnumerable());
            OnPropertyChanged("Customers");
        }

        public override void removeData()
        {
            Context = null;
           // Customers = null;
        }
        #endregion


        #region Methods
        void addNewCustomer()
        {
            ViewUC(() => new Views.Customers.AddEdit()
            {
                ID = $"Customer0",
                Title = $"New Customer"
            });
        }
      

        private void search()
        {
           if(SearchText!=null&& SearchText.Length>0) Searching = true;
        }

        private void cancelSearch()
        {
            SearchText = "";
            Searching = false;
        }
        void update()
        {
            if (SelectedCustomer == null) return;
            ViewUC(() => new Views.Customers.AddEdit(SelectedCustomer as Model.Customer, OpenMode.Modify, 0)
            {
                ID = $"Customer{((Model.Customer)selectedCustomer).ID}",
                Title = $"Customer {((Model.Customer)selectedCustomer).Name}"
            });
        }
        void deleteSelected()
        {
            if (SelectedCustomer == null) return;

        }

        private void displayCustomer(object page)
        {
            if (SelectedCustomer == null) return;
            ViewUC(() => new Views.Customers.AddEdit(SelectedCustomer as Model.Customer, OpenMode.Modify, int.Parse(page.ToString()))
            {
                ID = $"Customer{((Model.Customer)selectedCustomer).ID}",
                Title = $"Customer {((Model.Customer)selectedCustomer).Name}"
            });
        }
        #endregion
    }
}
