﻿using CVRM.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using MaterialDesignThemes.Wpf;
using System.Windows.Data;
using System.Globalization;
using System.Data.Entity;



namespace CVRM.VMs.Setting
{

    public class SettingsMainVM : BaseVM
    {
        #region ctor

        public SettingsMainVM()
        {
            this.OpenMode = OpenMode.Add;
            Page = 0;
        }
        public SettingsMainVM(Model.Customer Customer, OpenMode OpenMode, int Page)
        {
            this.OpenMode = OpenMode;
            this.Page = Page;
        }
        #endregion

        #region Properties
        private OpenMode openMode;

        public OpenMode OpenMode
        {
            get { return openMode; }
            set { openMode = value; OnPropertyChanged(); }
        }
        #region Activities
        private ObservableCollection<CVRM.Model.Activity> activities;

        public ObservableCollection<CVRM.Model.Activity> Activities
        {
            get { return activities; }
            set { activities = value; OnPropertyChanged(); }
        }
        private object selectedActivity;

        public object SelectedActivity
        {
            get { return selectedActivity; }
            set { selectedActivity = value;OnPropertyChanged(); }
        }
        private bool isAddActivityDialogOpened;

        public bool IsAddActivityDialogOpened
        {
            get { return isAddActivityDialogOpened; }
            set
            {
                isAddActivityDialogOpened = value;
                OnPropertyChanged();
            }
        }

        private Activity activityDialogObject;

        public Activity ActivityDialogObject
        {
            get { return activityDialogObject; }
            set
            {
                activityDialogObject = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Details
        private ObservableCollection<CVRM.Model.Detail> details;

        public ObservableCollection<CVRM.Model.Detail> Details
        {
            get { return details; }
            set { details = value; OnPropertyChanged(); }
        }
        private object selectedDetail;

        public object SelectedDetail
        {
            get { return selectedDetail; }
            set { selectedDetail = value; OnPropertyChanged(); }
        }

        private Detail detailDialogObject;

        public Detail DetailDialogObject
        {
            get { return detailDialogObject; }
            set
            {
                detailDialogObject = value;
                OnPropertyChanged();
            }
        }
        private bool isAddDetailDialogOpened;

        public bool IsAddDetailDialogOpened
        {
            get { return isAddDetailDialogOpened; }
            set
            {
                isAddDetailDialogOpened = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Jobs
        private ObservableCollection<CVRM.Model.Job> jobs;

        public ObservableCollection<CVRM.Model.Job> Jobs
        {
            get { return jobs; }
            set { jobs = value; OnPropertyChanged(); }
        }
        private object selectedJob;

        public object SelectedJob
        {
            get { return selectedJob; }
            set { selectedJob = value; OnPropertyChanged(); }
        }

        private Job jobDialogObject;

        public Job JobDialogObject
        {
            get { return jobDialogObject; }
            set
            {
                jobDialogObject = value;
                OnPropertyChanged();
            }
        }
        private bool isAddJobDialogOpened;

        public bool IsAddJobDialogOpened
        {
            get { return isAddJobDialogOpened; }
            set
            {
                isAddJobDialogOpened = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Documents
        private ObservableCollection<CVRM.Model.CustomerFileType> customerFileTypes;

        public ObservableCollection<CVRM.Model.CustomerFileType> CustomerFileTypes
        {
            get { return customerFileTypes; }
            set { customerFileTypes = value; OnPropertyChanged(); }
        }
        private object selectedCustomerFileType;

        public object SelectedCustomerFileType
        {
            get { return selectedCustomerFileType; }
            set { selectedCustomerFileType = value; OnPropertyChanged(); }
        }

        private CustomerFileType customerFileTypeDialogObject;

        public CustomerFileType CustomerFileTypeDialogObject
        {
            get { return customerFileTypeDialogObject; }
            set
            {
                customerFileTypeDialogObject = value;
                OnPropertyChanged();
            }
        }
        private bool isAddCustomerFileTypeDialogOpened;

        public bool IsAddCustomerFileTypeDialogOpened
        {
            get { return isAddCustomerFileTypeDialogOpened; }
            set
            {
                isAddCustomerFileTypeDialogOpened = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Documents
        private ObservableCollection<CVRM.Model.FileType> fileTypes;

        public ObservableCollection<CVRM.Model.FileType> FileTypes
        {
            get { return fileTypes; }
            set { fileTypes = value; OnPropertyChanged(); }
        }
        private object selectedFileType;

        public object SelectedFileType
        {
            get { return selectedCustomerFileType; }
            set { selectedCustomerFileType = value; OnPropertyChanged(); }
        }

        private FileType fileTypeDialogObject;

        public FileType FileTypeDialogObject
        {
            get { return fileTypeDialogObject; }
            set
            {
                fileTypeDialogObject = value;
                OnPropertyChanged();
            }
        }
        private bool isAddFileTypeDialogOpened;

        public bool IsAddFileTypeDialogOpened
        {
            get { return isAddFileTypeDialogOpened; }
            set
            {
                isAddFileTypeDialogOpened = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Data Lists

        #region FilteredData

        #endregion
        #endregion
        #region Data Objects
        #endregion
        #region Dialogs

        #region ConfirmDialog 

        private bool isConfirmDialogOpened;

        public bool IsConfirmDialogOpened
        {
            get { return isConfirmDialogOpened; }
            set
            {
                isConfirmDialogOpened = value;
                OnPropertyChanged();
            }
        }

        private string confirmDialogMessage;

        public string ConfirmDialogMessage
        {
            get { return confirmDialogMessage; }
            set
            {
                confirmDialogMessage = value;
                OnPropertyChanged();
            }
        }
        private object deletedObject;

        public object DeletedObject
        {
            get { return deletedObject; }
            set
            {
                deletedObject = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #endregion
        private int page;

        public int Page
        {
            get { return page; }
            set { page = value; OnPropertyChanged(); }
        }

        #endregion

        #region Commands
        #region Difinition
        public ObservableCollection<CommandVM> ConfirmDialogCommands { get; set; }
        public ObservableCollection<CommandVM> DetailDialogCommands { get; set; }
        public ObservableCollection<CommandVM> ActivityDialogCommands { get; set; }
        public ObservableCollection<CommandVM> JobDialogCommands { get; set; }
        public ObservableCollection<CommandVM> FileTypeDialogCommands { get; set; }
        public ObservableCollection<CommandVM> CustomerFileTypeDialogCommands { get; set; }

        #endregion

        #region CreateCommands
        public override ObservableCollection<CommandVM> CreateCommands()
        {
            ConfirmDialogCommands = new ObservableCollection<CommandVM>
            {
                //0-confirmDialogYes
                new CommandVM(new RelayCommand(param => confirmDialogYes())),
                //1-confirmDialogNo
                new CommandVM(new RelayCommand(param => confirmDialogNo())),
            };
            ActivityDialogCommands = new ObservableCollection<CommandVM>
            {
                //0-activityDialogAccept
                new CommandVM(new RelayCommand(param=>activityDialogAccept())),
                //0-activityDialogCancel
                new CommandVM(new RelayCommand(param=>activityDialogCancel())),
            };
            DetailDialogCommands = new ObservableCollection<CommandVM>
            {
                //0-detailDialogAccept
                new CommandVM(new RelayCommand(param=>detailDialogAccept())),
                //0-detailDialogCancel
                new CommandVM(new RelayCommand(param=>detailDialogCancel())),
            };
            JobDialogCommands = new ObservableCollection<CommandVM>
            {
                //0-jobDialogAccept
                new CommandVM(new RelayCommand(param=>jobDialogAccept())),
                //0-jobDialogCancel
                new CommandVM(new RelayCommand(param=>jobDialogCancel())),
            };
            CustomerFileTypeDialogCommands = new ObservableCollection<CommandVM>
            {
                //0-documentDialogAccept
                new CommandVM(new RelayCommand(param=>documentDialogAccept())),
                //0-documentDialogCancel
                new CommandVM(new RelayCommand(param=>documentDialogCancel())),
            };
            FileTypeDialogCommands = new ObservableCollection<CommandVM>
            {
                //0-fileTypeDialogAccept
                new CommandVM(new RelayCommand(param=>fileTypeDialogAccept())),
                //0-fileTypeDialogCancel
                new CommandVM(new RelayCommand(param=>fileTypeDialogCancel())),
            };
            return new ObservableCollection<CommandVM>
            {
                //0-Save
                new CommandVM(new RelayCommand (param=>save())),
                //1-cancel
                new CommandVM(new RelayCommand (param=>cancel())),
                //2-modifyBoxItem
                new CommandVM(new RelayCommand (param=>modifyBoxItem(param))),
                //3-deleteBoxItem
                new CommandVM(new RelayCommand (param=>deleteBoxItem(param))),
                //4-addBoxItem
                new CommandVM(new RelayCommand (param=>addBoxItem(param))),
            };
        }
        #endregion
        #endregion

        #region IData interface
        public override void getData()
        {
            Activities = new ObservableCollection<Activity>(Context.Activities.AsNoTracking().AsEnumerable());
            Details = new ObservableCollection<Detail>(Context.Details.AsNoTracking().AsEnumerable());
            Jobs = new ObservableCollection<Job>(Context.Jobs.AsNoTracking().AsEnumerable());
            CustomerFileTypes = new ObservableCollection<CustomerFileType>(Context.CustomerFileTypes.AsNoTracking().AsEnumerable());
            FileTypes = new ObservableCollection<FileType>(Context.FileTypes.AsNoTracking().AsEnumerable());
        }

        public override void removeData()
        {
            Context = null;
        }
        #endregion

        #region Methods
        void doNothing() { }
        #region Navigation
        async void save()
        {
            LoadingMessage = "Saving ...";
            startLoading();
            await Task.Run(() =>
            {
                
            });
            stopLoading();
            closeTab(true);
        }

        private void cancel()
        {
            closeTab();
        }

        private void changePage(object page)
        {
            Page = int.Parse(page.ToString());
        }
        #endregion

        #region Activity
        
        private void addBoxItem(object param)
        {
            switch (param.ToString())
            {
                case "activity":
                    ActivityDialogObject = new Activity();
                    IsAddActivityDialogOpened = true;
                    break;
                case "detail":
                    DetailDialogObject = new Detail();
                    IsAddDetailDialogOpened = true;
                    break;
                case "job":
                    JobDialogObject = new Job();
                    IsAddJobDialogOpened = true;
                    break;
                case "document":
                    CustomerFileTypeDialogObject = new CustomerFileType();
                    IsAddCustomerFileTypeDialogOpened = true;
                    break;
                case "fileType":
                    FileTypeDialogObject = new FileType();
                    IsAddFileTypeDialogOpened = true;
                    break;
                default:
                    break;
            }
        }
        private void activityDialogAccept()
        {
            if (ActivityDialogObject.ID > 0)
            {
                modifiyEntity(ActivityDialogObject);
                int index = Activities.IndexOf(SelectedActivity as Activity);
                Activities.Remove(SelectedActivity as Activity);
                Activities.Insert(index, ActivityDialogObject);
            }
            else
            {
                addEntity(ActivityDialogObject);
                Activities.Add(ActivityDialogObject);
            }
            IsAddActivityDialogOpened = false;
        }
        private void activityDialogCancel()
        {
            ActivityDialogObject = null;
            IsAddActivityDialogOpened = false;
        }
        private void fileTypeDialogAccept()
        {
            if (FileTypeDialogObject.ID > 0)
            {
                modifiyEntity(FileTypeDialogObject);
                int index = FileTypes.IndexOf(SelectedFileType as FileType);
                FileTypes.Remove(SelectedFileType as FileType);
                FileTypes.Insert(index, FileTypeDialogObject);
            }
            else
            {
                addEntity(FileTypeDialogObject);
                FileTypes.Add(FileTypeDialogObject);
            }
            IsAddFileTypeDialogOpened = false;
        }
        private void fileTypeDialogCancel()
        {
            FileTypeDialogObject = null;
            IsAddFileTypeDialogOpened = false;
        }
        private void documentDialogAccept()
        {
            if (CustomerFileTypeDialogObject.ID > 0)
            {
                modifiyEntity(CustomerFileTypeDialogObject);
                int index = CustomerFileTypes.IndexOf(SelectedCustomerFileType as CustomerFileType);
                CustomerFileTypes.Remove(SelectedCustomerFileType as CustomerFileType);
                CustomerFileTypes.Insert(index, CustomerFileTypeDialogObject);
            }
            else
            {
                addEntity(CustomerFileTypeDialogObject);
                CustomerFileTypes.Add(CustomerFileTypeDialogObject);
            }
            IsAddCustomerFileTypeDialogOpened = false;
        }
        private void documentDialogCancel()
        {
            CustomerFileTypeDialogObject = null;
            IsAddCustomerFileTypeDialogOpened = false;
        }
        private void jobDialogAccept()
        {
            if (JobDialogObject.ID > 0)
            {
                modifiyEntity(JobDialogObject);
                int index = Jobs.IndexOf(SelectedJob as Job);
                Jobs.Remove(SelectedJob as Job);
                Jobs.Insert(index, JobDialogObject);
            }
            else
            {
                addEntity(JobDialogObject);
                Jobs.Add(JobDialogObject);
            }
            IsAddJobDialogOpened = false;
        }
        private void jobDialogCancel()
        {
            JobDialogObject = null;
            IsAddJobDialogOpened = false;
        }
        private void detailDialogAccept()
        {
            if (DetailDialogObject.ID > 0)
            {
                modifiyEntity(DetailDialogObject);
                int index = Details.IndexOf(SelectedDetail as Detail);
                Details.Remove(SelectedDetail as Detail);
                Details.Insert(index, DetailDialogObject);
            }
            else
            {
                addEntity(DetailDialogObject);
                Details.Add(DetailDialogObject);
            }
            IsAddDetailDialogOpened = false;
        }
        private void detailDialogCancel()
        {
            DetailDialogObject = null;
            IsAddDetailDialogOpened = false;
        }
        #endregion
        private void modifyBoxItem(object param)
        {
            switch (param.ToString())
            {
                case "activity":
                    if (SelectedActivity == null) return;
                    ActivityDialogObject = Context.Activities.Find((SelectedActivity as Activity).ID);
                    IsAddActivityDialogOpened = true;
                    break;
                case "detail":
                    if (SelectedDetail == null) return;
                    DetailDialogObject = Context.Details.Find((SelectedDetail as Detail).ID);
                    IsAddDetailDialogOpened = true;
                    break;
                case "job":
                    if (SelectedJob == null) return;
                    JobDialogObject = Context.Jobs.Find((SelectedJob as Job).ID);
                    IsAddJobDialogOpened = true;
                    break;
                case "document":
                    if (SelectedCustomerFileType == null) return;
                    CustomerFileTypeDialogObject = Context.CustomerFileTypes.Find((SelectedCustomerFileType as CustomerFileType).ID);
                    IsAddCustomerFileTypeDialogOpened = true;
                    break;
                case "fileType":
                    if (SelectedFileType == null) return;
                    FileTypeDialogObject = Context.FileTypes.Find((SelectedFileType as FileType).ID);
                    IsAddFileTypeDialogOpened = true;
                    break;
                default:
                    break;
            }
        }
        private void deleteBoxItem(object param)
        {
            switch (param.ToString())
            {
                case "activity":
                    if (SelectedActivity == null) return;
                    DeletedObject = SelectedActivity;
                    ConfirmDialogMessage = $"Confirm delete activity {(SelectedActivity as Activity).Name}  ?";
                    break;
                case "detail":
                    if (SelectedDetail == null) return;
                    DeletedObject = SelectedDetail;
                    ConfirmDialogMessage = $"Confirm delete Detail {(SelectedDetail as Detail).Name}  ?";
                    break;
                case "job":
                    if (SelectedJob == null) return;
                    DeletedObject = SelectedJob;
                    ConfirmDialogMessage = $"Confirm delete job {(SelectedJob as Job).Name}  ?";
                    break;
                case "document":
                    if (SelectedCustomerFileType == null) return;
                    DeletedObject = SelectedCustomerFileType;
                    ConfirmDialogMessage = $"Confirm delete Document type {(SelectedCustomerFileType as CustomerFileType).Name}  ?";
                    break;
                case "fileType":
                    if (SelectedFileType == null) return;
                    DeletedObject = SelectedFileType;
                    ConfirmDialogMessage = $"Confirm delete Document type {(SelectedFileType as FileType).Name}  ?";
                    break;
                default:
                    break;
            }
            IsConfirmDialogOpened = true;
        }
        private void confirmDialogYes()
        {
            if (DeletedObject is Activity)
            {
                deleteEntity(DeletedObject as Activity);
                Activities.Remove(DeletedObject as Activity);
            }
            if (DeletedObject is Detail)
            {
                deleteEntity(DeletedObject as Detail);
                Details.Remove(DeletedObject as Detail);
            }
            if (DeletedObject is Job)
            {
                deleteEntity(DeletedObject as Job);
                Jobs.Remove(DeletedObject as Job);
            }
            if (DeletedObject is CustomerFileType)
            {
                deleteEntity(DeletedObject as CustomerFileType);
                CustomerFileTypes.Remove(DeletedObject as CustomerFileType);
            }
            if (DeletedObject is FileType)
            {
                deleteEntity(DeletedObject as FileType);
                FileTypes.Remove(DeletedObject as FileType);
            }
            IsConfirmDialogOpened = false;
            DeletedObject = null;
        }

        private void confirmDialogNo()
        {
            IsConfirmDialogOpened = false;
            DeletedObject = null;
        }
        #endregion
    }
}
