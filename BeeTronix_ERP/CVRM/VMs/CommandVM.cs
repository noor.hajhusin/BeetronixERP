﻿using System;
using System.Windows.Input;

namespace CVRM.VMs
{
    public class CommandVM
    {
        #region Fields
        /// <summary>
        /// Represet the Command that will be execute
        /// </summary>
        public ICommand Command { get; private set; }
        /// <summary>
        /// Represent the name (type) of the View that this command will display
        /// </summary>
        private string displayView;

        public string DisplayView
        {
            get { return displayView; }
            set { displayView = value; }
        }

        #endregion
        #region Costructors
        /// <summary>
        /// Create new Command With Display Name and Command Logic
        /// </summary>
        /// <param name="displayView">The Window or usercontrol that the command will interact with </param>
        /// <param name="command">Command Method or logic</param>
        public CommandVM(string displayView, ICommand command)
        {
            if (command == null)
                throw new ArgumentNullException("No Command Found");
            Command = command;
            DisplayView = displayView;
        }
        /// <summary>
        ///  Create new Command With Display Name and Command Logic
        /// </summary>
        /// <param name="command">Command Method or logic</param>
        public CommandVM(ICommand command)
        {
            if (command == null)
                throw new ArgumentNullException("No Command Found");
            Command = command;
            DisplayView = null;
        }
        #endregion
    }

}

