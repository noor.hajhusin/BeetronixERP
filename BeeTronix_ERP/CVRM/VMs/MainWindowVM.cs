﻿using CVRM.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace CVRM.VMs
{
    public class MainWindowVM : BaseVM<MainWindowVM>
    {
        #region Fields
        private bool isMenuOpen;

        public bool IsMenuOpen
        {
            get { return isMenuOpen; }
            set { isMenuOpen = value; OnPropertyChanged(); }
        }


        #region GUIs

        #region DependencyProperties

        #endregion

        #endregion

        #region Filtered Lists

        #region Search Strings
        #endregion

        #region Lists
        #endregion

        #endregion

        #region Data Lists


        #endregion

        #region Selected Data
        #endregion




        #endregion

        #region Commands

        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>()
            {
                //0-home
                new CommandVM(new RelayCommand(c=>home())),
                //1-openCustomersMain
                new CommandVM(new RelayCommand(c=>openCustomersMain())),
                //2-openSettings
                new CommandVM(new RelayCommand(c=>openSettings())),
                //3-openCustomersMain
                new CommandVM(new RelayCommand(c=>openCustomersMain())),
                //4-closeTab
                new CommandVM(new RelayCommand(c=>closeTab())),
                //5-openProducts
                new CommandVM(new RelayCommand(c=>openProducts())),
                //6-openEmployees
                new CommandVM(new RelayCommand(c=>openEmployees())),
                //7-openPeople
                new CommandVM(new RelayCommand(c=>openPeople())),
                //8-openPlaces
                new CommandVM(new RelayCommand(c=>openPlaces())),
            };
        }

        #endregion

        #region Constructors

        public MainWindowVM()
        {
        }
        #endregion

        #region Methods

        public override void getData()
        {

        }

        public override void removeData()
        {

        }

        void home()
        {

        }
        void openCustomersMain()
        {
            ViewUC(() => new Views.Customers.Index());
            IsMenuOpen = false;
        }
        void openSettings()
        {
            ViewUC(() => new Views.Settings.Main());
            IsMenuOpen = false;
        }
        void closeTab()
        {
            base.closeTab();
        }
        private void openProducts()
        {
            ViewUC(() => new Views.Product.Index());
            IsMenuOpen = false;
        }
        private void openEmployees()
        {
            ViewUC(() => new Views.Employee.Index());
            IsMenuOpen = false;
        }
        private void openPeople()
        {
            ViewUC(() => new Views.Person.Index());
            IsMenuOpen = false;
        }
        private void openPlaces()
        {
            ViewUC(() => new Views.Places.Index());
            IsMenuOpen = false;
        }
        #endregion
    }
}