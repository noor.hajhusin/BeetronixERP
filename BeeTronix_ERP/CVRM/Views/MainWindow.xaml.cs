﻿using CVRM.Model;
using CVRM.Model.Migrations;
using CVRM.VMs;
using CVRM.VMs.GUITools.CustomControls;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CVRM
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = VM;
            setPanels();
            updateDB();
        }

        async void updateDB()
        {
            VM.LoadingMessage = "Updating Database ...";
            VM.startLoading(true);
            DataContext = null;
            await Task.Run(() =>
            {
                IDatabaseInitializer<CVRM_Context> initializer = new MigrateDatabaseToLatestVersion<CVRM_Context, Configuration>();
                Database.SetInitializer(new MigrateDatabaseToLatestVersion<CVRM_Context, Configuration>());
               // var test = vm.Context.Activities.ToList();
            });
            this.DataContext = VM;
            VM.stopLoading();
            VM.enableNavigation();
        }

        private void setPanels()
        {
            MainWindowVM.LoadingPanel = loadingPanel;
            MainWindowVM.LoadingTextBlock = loadingTextBlock;
            MainWindowVM.TabsPanel = tab;
            MainWindowVM.NavigationControls.Add(MenuToggleButton);
            MainWindowVM.NavigationControls.Add(popUp);
        }

        MainWindowVM vm;
        public MainWindowVM VM
        {
            get { if (vm == null) vm = new MainWindowVM(); return vm; }
            set { vm = value; }
        }
    }
}
