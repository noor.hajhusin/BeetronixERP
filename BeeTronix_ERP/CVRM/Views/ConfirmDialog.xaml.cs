﻿using CVRM.VMs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CVRM.Views
{
    /// <summary>
    /// Interaction logic for ConfirmDialog.xaml
    /// </summary>
    public partial class ConfirmDialog : UserControl,INotifyPropertyChanged
    {
        public ConfirmDialog()
        {
            InitializeComponent();
           message.DataContext = this;
        }

        #region Commands

        public ICommand AcceptCommand
        {
            get { return (ICommand)GetValue(AcceptCommandProperty); }
            set { SetValue(AcceptCommandProperty, value); }
        }

        public static readonly DependencyProperty AcceptCommandProperty =
            DependencyProperty.Register("AcceptCommand", typeof(ICommand), typeof(ConfirmDialog), new PropertyMetadata(default(ICommand)));

        public object AcceptCommandParameter
        {
            get { return (object)GetValue(AcceptCommandParameterProperty); }
            set { SetValue(AcceptCommandParameterProperty, value); }
        }

        public static readonly DependencyProperty AcceptCommandParameterProperty =
            DependencyProperty.Register("AcceptCommandParameter", typeof(object), typeof(ConfirmDialog), new PropertyMetadata(default(object)));

        public ICommand CancelCommand
        {
            get { return (ICommand)GetValue(CancelCommandProperty); }
            set { SetValue(CancelCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CancelCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CancelCommandProperty =
            DependencyProperty.Register("CancelCommand", typeof(ICommand), typeof(ConfirmDialog), new PropertyMetadata(default(ICommand)));

        public object CancelCommandParameter
        {
            get { return (object)GetValue(CancelCommandParameterProperty); }
            set { SetValue(CancelCommandParameterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CancelCommandParameter.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CancelCommandParameterProperty =
            DependencyProperty.Register("CancelCommandParameter", typeof(object), typeof(ConfirmDialog), new PropertyMetadata(default(object)));

        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set {SetValue(MessageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(ConfirmDialog), new PropertyMetadata(default(string)));




        #endregion
        public event PropertyChangedEventHandler PropertyChanged;

        private void accept(object sender, RoutedEventArgs e)
        {
            if(AcceptCommand!=null) AcceptCommand.Execute(AcceptCommandParameter);
        }
        private void cancel(object sender, RoutedEventArgs e)
        {
            if (CancelCommand != null) CancelCommand.Execute(CancelCommandParameter);
        }
    }
}
