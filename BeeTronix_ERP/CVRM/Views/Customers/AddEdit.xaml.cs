﻿using CVRM.Model;
using CVRM.VMs.GUITools.CustomControls;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CVRM.VMs;
using CVRM.VMs.Customer;
using System.IO;

namespace CVRM.Views.Customers
{
    /// <summary>
    /// Interaction logic for Index.xaml
    /// </summary>
    public partial class AddEdit : BaseUC
    {
        #region Fields
        private AddEditVM vm;

        public AddEditVM VM
        {
            get { if (vm == null) vm = new AddEditVM(); return vm;  }
            set { vm = value; }
        }

        public override BaseVM ViewModel
        {
            get { if (vm == null) vm = new AddEditVM(); return vm; }
            set { vm = (AddEditVM)value; }
        }
        #endregion
        #region Ctors

        public AddEdit()
        {
            InitializeComponent();
            DataContext = VM;
        }
        public AddEdit(Customer customer,OpenMode openMode,int Page)
        {
            InitializeComponent();
            vm = new AddEditVM(customer, openMode, Page);
            DataContext = VM;
        }
        #endregion
        

        private void dGridRow_Files_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            VM.CmdLst[29].Command.Execute("file");
        }
        private void dGridRow_Log_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            VM.CmdLst[29].Command.Execute("call");
        }
        private void dGridRow_Person_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            VM.CmdLst[29].Command.Execute("person");
        }

        private void selectRow(object sender, MouseButtonEventArgs e)
        {
            if (sender is DataGridRow)
                ((DataGridRow)sender).IsSelected = true;
            else if (sender is ListBoxItem)
                ((ListBoxItem)sender).IsSelected = true;
        }

        private void LoadUC(object sender, RoutedEventArgs e)
        {

        }
    }//
}
