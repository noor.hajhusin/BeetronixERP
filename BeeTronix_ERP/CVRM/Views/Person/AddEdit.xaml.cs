﻿using CVRM.VMs.GUITools.CustomControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CVRM.VMs.Person;
using CVRM.VMs;

namespace CVRM.Views.Person
{
    /// <summary>
    /// Interaction logic for AddEdit.xaml
    /// </summary>
    public partial class AddEdit : BaseUC
    {

        #region Fields
        private AddEditVM vm;
        public AddEditVM VM
        {
            get { if (vm == null) vm = new AddEditVM(); return vm; }
            set { vm = value; }
        }

        public override BaseVM ViewModel
        {
            get
            {
                if (vm == null) vm = new AddEditVM();
                return vm;
            }

            set
            {
                vm = (AddEditVM)value;
            }
        }


        #endregion

        #region cotr

        public AddEdit()
        {
            InitializeComponent();
            DataContext = VM;
        }
        public AddEdit(Model.Person person)
        {
            InitializeComponent();
            vm = new AddEditVM(person);
            DataContext = VM;
        }

        #endregion

    }
}
