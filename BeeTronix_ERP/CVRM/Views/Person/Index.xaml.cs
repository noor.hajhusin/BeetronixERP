﻿using CVRM.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using CVRM.VMs.GUITools.CustomControls;
using CVRM.VMs;
using CVRM.VMs.Customer;

namespace CVRM.Views.Person
{

    /// <summary>
    /// Interaction logic for Index.xaml
    /// </summary>
    public partial class Index : BaseUC
    {
        #region Fields
        private IndexVM vm;

        public IndexVM VM
        {
            get { if (vm==null)vm= new IndexVM(); return vm; }
            set { vm = value; }
        }

        public override BaseVM ViewModel
        {
            get { if (vm == null) vm = new IndexVM(); return vm; }
            set { vm = (IndexVM)value; }
        } 
        #endregion

        public Index()
        {
            InitializeComponent();
            DataContext = VM;
        }

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            VM.CmdLst[5].Command.Execute(0);
        }

        private void selectRow(object sender, MouseButtonEventArgs e)
        {
            ((DataGridRow)sender).IsSelected = true;
        }
    }
}
