﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CVRM.Views.Call
{
    /// <summary>
    /// Interaction logic for AddCallDialog.xaml
    /// </summary>
    public partial class AddReminderDialog : UserControl
    {
        public AddReminderDialog()
        {
            InitializeComponent();
        }
        private void selectRow(object sender, MouseButtonEventArgs e)
        {
            if (sender is DataGridRow)
                ((DataGridRow)sender).IsSelected = true;
            else if (sender is ListBoxItem)
                ((ListBoxItem)sender).IsSelected = true;
        }
    }
}
