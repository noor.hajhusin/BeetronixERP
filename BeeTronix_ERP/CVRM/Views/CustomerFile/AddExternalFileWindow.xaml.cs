﻿using CVRM.VMs.GUITools.CustomControls;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CVRM.VMs;
using CVRM.VMs.CustomerFile;

namespace CVRM.Views.CustomerFile
{
    /// <summary>
    /// Interaction logic for AddExternalFileWindow.xaml
    /// </summary>
    public partial class AddExternalFileWindow : MetroWindow,IController
    {
        private AddExternalFileWindowVM vm;

        public AddExternalFileWindowVM VM
        {
            get { if (vm == null) vm = new AddExternalFileWindowVM(FilePath); return vm; }
            set { vm = value; }
        }

        string FilePath;
        public AddExternalFileWindow(string filePath)
        {
            InitializeComponent();
            FilePath = filePath;
            setPanels();
            this.DataContext = VM;
            Refresh();
        }

        private void setPanels()
        {
            AddExternalFileWindowVM.LoadingPanel = loadingPanel;
            AddExternalFileWindowVM.LoadingTextBlock = loadingTextBlock;
        }
        public BaseVM ViewModel
        {
            get { if (vm == null) vm = new AddExternalFileWindowVM(FilePath); return vm; }
            set { vm = (AddExternalFileWindowVM)value; }
        }

        public async void Refresh()
        {
            await Task.Run(() =>
            {
                ViewModel.getData();
            });
        }

        public void RemoveData()
        {
            //Safty condition (in case that the view Does not has a View model)
            if (ViewModel == null)
            {
                return;
            }
            //Remove the Data from the VM
            ViewModel.removeData();
        }
    }
}
