﻿using CVRM.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CVRM.Views.CustomerFile
{
    /// <summary>
    /// Interaction logic for AddCustomerFileDialog.xaml
    /// </summary>
    public partial class AddCustomerFileDialog : UserControl
    {
        public AddCustomerFileDialog()
        {
            InitializeComponent();
        }
        private void dropFile(object sender, DragEventArgs e)
        {
            loadFile(e);
            Focus();
        }
        async void loadFile(DragEventArgs e)
        {
            VMs.Customer.AddEditVM VM = DataContext as VMs.Customer.AddEditVM;
            VM.startLoading();
            await Task.Run(() =>
            {
                string v = ((string[])e.Data.GetData(DataFormats.FileDrop, true))[0];
                VM.FileDialogObject.File = new Model.File { Name = System.IO.Path.GetFileNameWithoutExtension(v), Path = v };
                FileStream fs = new FileStream(v, FileMode.Open, FileAccess.Read);
                VM.FileDialogObject.File.Data = new byte[fs.Length];
                fs.Read(VM.FileDialogObject.File.Data, 0, System.Convert.ToInt32(fs.Length));
                fs.Close();
                string ext = System.IO.Path.GetExtension(v).Remove(0, 1);
                Model.FileType ft = VM.FileTypes.Where(c => c.Extension.ToUpper().Contains(ext.ToUpper())).FirstOrDefault();
                VM.FileDialogObject.File.FileType = ft == null ? VM.FileDialogObject.File.FileType : ft;
                VM.OnPropertyChanged("FileDialogObject");
            });
            rtg_Back.Visibility = Visibility.Collapsed;
            VM.stopLoading();
        }

        private void drag(object sender, DragEventArgs e)
        {
            rtg_Back.Visibility = Visibility.Visible;
        }

        private void leave(object sender, DragEventArgs e)
        {
            rtg_Back.Visibility = Visibility.Collapsed;
        }
    }
}
