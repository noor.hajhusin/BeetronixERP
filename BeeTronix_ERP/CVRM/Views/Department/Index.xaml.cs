﻿using CVRM.VMs;
using CVRM.VMs.Department;
using CVRM.VMs.GUITools.CustomControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CVRM.Views.Department
{
    /// <summary>
    /// Interaction logic for Index.xaml
    /// </summary>
    public partial class Index : BaseUC
    {
       

        private IndexVM vm;

        public IndexVM VM
        {
            get { if (vm == null) vm = new IndexVM(); return vm; }
            set { vm = value; }
        }
        public Index()
        {
            InitializeComponent();
            DataContext = VM;
        }
        public override BaseVM ViewModel
        {
            get { if (vm == null) vm = new IndexVM(); return vm; }
            set { vm = (IndexVM)value; }
        }
        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            VM.CmdLst[5].Command.Execute(0);
        }

        private void selectRow(object sender, MouseButtonEventArgs e)
        {
            ((DataGridRow)sender).IsSelected = true;
        }
    }
}
