﻿using CVRM.VMs.GUITools.CustomControls;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CVRM.VMs;

namespace CVRM.Views
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : MetroWindow,IController
    {
        LoginWindowVM vm;
        LoginWindowVM VM
        {
            get
            {
                if (vm == null) vm = new LoginWindowVM();
                return vm;
            }
            set { vm = value; }
        }
        static LoginWindow window;
        public static LoginWindow Window
        {
            get
            {
                if (window == null) window = new LoginWindow();
                return window;
            }
            private set { window = value; }
        }
        
        public LoginWindow()
        {
            InitializeComponent();
            DataContext = VM;
            setPanels();
            Refresh();
        }

        public BaseVM ViewModel
        {
            get
            {
                if (vm == null) vm = new LoginWindowVM();
                return vm;
            }
            set { vm = (LoginWindowVM)value; }
        }

        public async void Refresh()
        {
            ViewModel.LoadingMessage = "Loading";
            ViewModel.startLoading(true);
            await Task.Run(() =>
            {
                ViewModel.getData();
            });
            ViewModel.stopLoading();
            ViewModel.enableNavigation();
        }

        public void RemoveData()
        {

        }

        private void setPanels()
        {
            LoginWindowVM.LoadingPanel = loadingPanel;
            LoginWindowVM.LoadingTextBlock = loadingTextBlock;
           // LoginWindowVM.NavigationControls.Add(MenuToggleButton);
        }

        private void loadWindow(object sender, RoutedEventArgs e)
        {
            passwordBox.Focus();
        }
    }
}
