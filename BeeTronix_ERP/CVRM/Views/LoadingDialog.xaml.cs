﻿using CVRM.VMs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CVRM.Views
{
    /// <summary>
    /// Interaction logic for ConfirmDialog.xaml
    /// </summary>
    public partial class LoadingDialog : UserControl,INotifyPropertyChanged
    {
        public LoadingDialog()
        {
            InitializeComponent();
           message.DataContext = this;
        }

        public async void Start() {
           await new Task(Method);
        }

        #region Commands
        
        public ICommand CancelCommand
        {
            get { return (ICommand)GetValue(CancelCommandProperty); }
            set { SetValue(CancelCommandProperty, value); }
        }

        public static readonly DependencyProperty CancelCommandProperty =
            DependencyProperty.Register("CancelCommand", typeof(ICommand), typeof(LoadingDialog), new PropertyMetadata(default(ICommand)));

        public object CancelCommandParameter
        {
            get { return (object)GetValue(CancelCommandParameterProperty); }
            set { SetValue(CancelCommandParameterProperty, value); }
        }

        public static readonly DependencyProperty CancelCommandParameterProperty =
            DependencyProperty.Register("CancelCommandParameter", typeof(object), typeof(LoadingDialog), new PropertyMetadata(default(object)));

        public  string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(LoadingDialog), new PropertyMetadata(default(string)));

        public Action Method
        {
            get { return (Action)GetValue(MethodProperty); }
            set { SetValue(MethodProperty, value); }
        }

        public static readonly DependencyProperty MethodProperty =
            DependencyProperty.Register("Method", typeof(string), typeof(LoadingDialog), new PropertyMetadata(default(Action)));




        #endregion
        public event PropertyChangedEventHandler PropertyChanged;

        private void cancel(object sender, RoutedEventArgs e)
        {
            if(CancelCommand!=null) CancelCommand.Execute(CancelCommandParameter);
        }
    }
}
