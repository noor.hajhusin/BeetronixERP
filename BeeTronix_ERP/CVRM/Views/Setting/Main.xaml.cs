﻿using CVRM.Model;
using CVRM.VMs.GUITools.CustomControls;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CVRM.VMs;
using CVRM.VMs.Setting;
using System.IO;

namespace CVRM.Views.Settings
{
    /// <summary>
    /// Interaction logic for Index.xaml
    /// </summary>
    public partial class Main : BaseUC
    {
        #region Fields
        private SettingsMainVM vm;

        public SettingsMainVM VM
        {
            get { if (vm == null) vm = new SettingsMainVM(); return vm;  }
            set { vm = value; }
        }

        public override BaseVM ViewModel
        {
            get { if (vm == null) vm = new SettingsMainVM(); return vm; }
            set { vm = (SettingsMainVM)value; }
        }
        #endregion
        #region Ctors

        public Main()
        {
            InitializeComponent();
            DataContext = VM;
        }
        public Main(Customer customer,OpenMode openMode,int Page)
        {
            InitializeComponent();
            vm = new SettingsMainVM(customer, openMode, Page);
            DataContext = VM;
        }
        #endregion
        
        
        private void detailItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
           // VM.CmdLst[2].Command.Execute("detail");
        }
        private void activityItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
           // VM.CmdLst[2].Command.Execute("activity");
        }

        private void select(object sender, MouseButtonEventArgs e)
        {
            if (sender is DataGridRow)
                ((DataGridRow)sender).IsSelected = true;
            else if (sender is ListBoxItem)
                ((ListBoxItem)sender).IsSelected = true;
        }

        private void LoadUC(object sender, RoutedEventArgs e)
        {

        }
    }//
}
