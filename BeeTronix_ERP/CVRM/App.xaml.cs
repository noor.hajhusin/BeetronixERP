﻿using CVRM.Model;
using CVRM.Model.Migrations;
using CVRM.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Windows;

namespace CVRM
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
       public static string[] args { get; set; }

        public App()
        {
            
        }

        private void startupApp(object sender, StartupEventArgs e)
        {
            //args = e.Args;
            //LoginWindow LoginWindow = new LoginWindow();
            //LoginWindow.Show();
            //if (e.Args.Count() > 0)
            //{
            //    new Views.CustomerFile.AddExternalFileWindow(App.args[0]).Show();
            //}
            //else
            //{
            //    new MainWindow().Show();
            //}
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            new Views.ExeptionWindow().ShowDialog();
        }
    }
}
